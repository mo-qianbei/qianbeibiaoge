Attribute VB_Name = "callBack"
Option Explicit


'=======================
'
'     公共内部函数
'
'=======================

' 判断是否工作表已存在
Private Function hasWorkSheet(ByVal shtName As String) As Boolean
    Dim wsht
    On Error Resume Next
    Set wsht = Worksheets(shtName)
    If Err.Number = 0 Then
        hasWorkSheet = True
    Else
        hasWorkSheet = False
    End If
End Function


' 写入文本内容到剪贴板
Public Function CopyToClipboard(str As String) As String

    With CreateObject("Forms.TextBox.1")
        .MultiLine = True
        .text = str
        .SelStart = 0
        .SelLength = .TextLength
        .Copy
    End With
    CopyToClipboard = str
End Function

' s 暂停的秒数，1秒 = 1000毫秒
Sub Sleep(Optional ByVal s As Single = 1)

    Dim StartTime As Single
    StartTime = Timer   ' 获取今天过了多少秒
    Dim StartDate As Date
    StartDate = Date    ' 获取当前日期

    Do While 86400 * (Date - StartDate) + Timer < StartTime + s
        DoEvents
    Loop
End Sub



'=======================
'  单 元 格 数 字 格 式
'=======================

'单元格显示为万亿KM
' 参考：https://club.excelhome.net/thread-1312477-1-1.html -------------------------------
'1、万位保留1位小数：自定义格式应设置为0!.0,"万"或0"."#,"万" 或0"."0,"万"
'2、万位保留0位小数，但有千分符：自定义格式应设置为#,##0,,"万"%
'3、万位保留1位小数，且有千分符：自定义格式应设置为#,##0.0,,"万"%
'4、万位保留2位小数，且有千分符：自定义格式应设置为#,##0.00,,"万"%
'如果不需要单位，去掉“"万"”（单位及对应双引号）

'关于#,##0,,%自定义格式的设置说明：
'1.输入#,##0,,%→鼠标将光标移至%前→按住Alt键的同时，依次按选小键盘的数字1和0（或者使用Ctrl+J）
'2.将选定对象设定为“自动换行”隐藏最后的“%”
'--------------------------------------------------------------------------------------------
Private Sub digitalFormat(ByVal numberscale As String, Optional accuracy As Byte = 2, Optional hasSeparator As Boolean = False)
    With Selection
        Select Case UCase(numberscale)
            Case "K"
                Select Case accuracy
                    Case 0
                        .NumberFormatLocal = "0,""K"""     '"在VBA中需要使用""转义
                    Case 1
                        .NumberFormatLocal = "0.0,""K"""
                    Case 2
                        .NumberFormatLocal = "0.00,""K"""
                    Case Else
                End Select
            Case "M"
                Select Case accuracy
                    Case 0
                       .NumberFormatLocal = "0,,""M"""
                    Case 1
                       .NumberFormatLocal = "0.0,,""M"""
                    Case 2
                       .NumberFormatLocal = "0.00,,""M"""
                    Case Else
                End Select
            Case "W"
                If hasSeparator Then    '有千分符
                    Select Case accuracy
                        Case 0
                            .NumberFormatLocal = "#,##0,,""万""" & Chr(10) & "%"   ' chr(10)在单元格内回车（Ctrl+J）
                        Case 1
                            .NumberFormatLocal = "#,##0.0,,""万""" & Chr(10) & "%"
                        Case 2
                            .NumberFormatLocal = "#,##0.00,,""万""" & Chr(10) & "%"
                        Case Else
                    End Select
                Else                    '无千分符
                    Select Case accuracy
                        Case 0
                            .NumberFormatLocal = "0,,""万""" & Chr(10) & "%"
                        Case 1
                            .NumberFormatLocal = "0,,.0""万""" & Chr(10) & "%"
                        Case 2
                            .NumberFormatLocal = "0,,.00""万""" & Chr(10) & "%"
                        Case Else
                    End Select
                End If
            Case "HM"
                .NumberFormatLocal = "0!.00,,""亿"""
        End Select
        
        .WrapText = True   '自动换行
        .EntireRow.AutoFit '自动行高
        .VerticalAlignment = xlTop ' 垂直靠上对齐
    End With
End Sub
Sub digitalK0(control As IRibbonControl)
    Call digitalFormat("K", 0)
End Sub
Sub digitalK1(control As IRibbonControl)
    Call digitalFormat("K", 1)
End Sub
Sub digitalK2(control As IRibbonControl)
    Call digitalFormat("K", 2)
End Sub
Sub digitalW0(control As IRibbonControl)
    Call digitalFormat("W", 0)
End Sub
Sub digitalW1(control As IRibbonControl)
    Call digitalFormat("W", 1)
End Sub
Sub digitalW2(control As IRibbonControl)
    Call digitalFormat("W", 2)
End Sub
Sub digitalM0(control As IRibbonControl)
    Call digitalFormat("M", 0)
End Sub
Sub digitalM1(control As IRibbonControl)
    Call digitalFormat("M", 1)
End Sub
Sub digitalM2(control As IRibbonControl)
    Call digitalFormat("M", 2)
End Sub
Sub digitalHM(control As IRibbonControl)    '亿，只能设置为2位小数
    Call digitalFormat("HM")
End Sub
' 自动设置个万亿
Sub digitalAuto(control As IRibbonControl)
    Dim rng As Range
    For Each rng In Selection.Cells
        If Abs(rng.Value) < 10 ^ 4 Then
            rng.NumberFormatLocal = "0"
        ElseIf Abs(rng.Value) < 10 ^ 8 Then
            rng.NumberFormatLocal = "0!.0,""万"""
        Else
            rng.NumberFormatLocal = "0!.00,,""亿"""
        End If
    Next
    Selection.VerticalAlignment = xlTop
End Sub

'=======================
'  数 字 转 日 期 格 式
'=======================

Sub dateAcross(control As IRibbonControl)
    Selection.NumberFormatLocal = "yyyy-mm-dd;@"
End Sub
Sub dateText(control As IRibbonControl)
    Selection.NumberFormatLocal = "yyyy年mm月dd日;@"
End Sub

'=======================
'  单 元 格 文 本 处 理
'=======================

' 支持多个或单个单元格
' 使用数组，但限制区域必须连续成矩形
'Sub addPrefix(control As IRibbonControl)
'    Dim prefix As String
'    On Error Resume Next
'    prefix = InputBox("请输入要添加的前缀：")
'    If Err Then Exit Sub
'
'    Dim arr As Variant
'    If Selection.Cells.Count > 1 Then
'        arr = Selection.Value
'        ' 使用数组提高遍历效率
'        Dim i As Long, j As Long
'        For i = LBound(arr) To UBound(arr)
'            For j = LBound(arr, 2) To UBound(arr, 2)
'                arr(i, j) = prefix & arr(i, j)
'            Next
'        Next
'    Else
'        arr = prefix & arr
'    End If
'    Selection = arr
'End Sub

Sub addPrefix(control As IRibbonControl)

    On Error Resume Next
    Dim prefix As String
    prefix = InputBox("请输入要添加的前缀：")
    If Err Then Exit Sub
    Dim c As Range
    For Each c In Selection.Cells
        c.Value = prefix & c.Value
    Next

End Sub


Sub addSuffix(control As IRibbonControl)
    ' 如果是点选的，单元格不在一起，使用数组会出现问题

    Dim suffix As String
    On Error Resume Next
    suffix = InputBox("请输入要添加的后缀：")
    If Err Then Exit Sub
 
    Dim c As Range
    For Each c In Selection.Cells
        c.Value = c.Value & suffix
    Next

    ' Selection.EntireColumn.AutoFit
End Sub
'转为日期
Sub transferDate(control As IRibbonControl)

    Dim rng As Range
    Set rng = Selection
    Dim findArr() As Variant
    findArr = Array("年", "月", "~", "。", "、") '注意没有“日”
    Dim i As Long
    For i = 1 To rng.Columns.Count
        '使用分列方式转换字符串，可以将“20210104”转换为对应日期
        Selection.Columns(i).TextToColumns DataType:=xlDelimited, FieldInfo:=Array(1, 5), TrailingMinusNumbers:=True
              'xlDelimited使用分隔符分割
              'TextQualifier是使用单引号、双引号还是不使用引号作为文本限定符
        Selection.Columns(i).AutoFit    '自动调整列宽，防止出现####错误
    Next

    For i = LBound(findArr) To UBound(findArr)
        Selection.Replace What:=findArr(i), Replacement:="/", LookAt:=xlPart, _
            SearchOrder:=xlByRows, SearchFormat:=False, _
            ReplaceFormat:=False
    Next
    Selection.Replace What:="日", Replacement:="", LookAt:=xlPart, _
        SearchOrder:=xlByRows, SearchFormat:=False, _
        ReplaceFormat:=False
    Selection.Replace What:="号", Replacement:="", LookAt:=xlPart, _
        SearchOrder:=xlByRows, SearchFormat:=False, _
        ReplaceFormat:=False

    Selection.NumberFormatLocal = "yyyy-mm-dd;@"

End Sub

'千万亿KM转真正数字()
Sub transferNumber(control As IRibbonControl)

    If MsgBox("请确认所选的每个单元格满足以下条件" & Chr(10) & _
      "    1.至多包含“万亿KM”中的一个单位，该单位位于末尾" & Chr(10) & _
      "    2.不能出现其他符号，如“,￥$”等", vbOKCancel) = vbOK Then

        Dim rng As Range
        For Each rng In Selection
            Dim str As String
            str = Right(rng.Value, 1)
            Select Case UCase(str)
                Case "K", "千"
                    rng.Value = VBA.Strings.Left(rng, VBA.Strings.Len(rng) - 1) * 1000
                    Selection.NumberFormatLocal = "0.00,""K"""
                Case "M"
                    rng.Value = VBA.Strings.Left(rng, VBA.Strings.Len(rng) - 1) * 1000000#
                    rng.NumberFormatLocal = "0.00,,""M"""
                Case "亿"
                    rng.Value = VBA.Strings.Left(rng, VBA.Strings.Len(rng) - 1) * 100000000#
                    rng.NumberFormatLocal = "0!.00,,""亿"""
                Case "W", "万"
                    rng.Value = VBA.Strings.Left(rng, VBA.Strings.Len(rng) - 1) * 10000#
                    rng.NumberFormatLocal = "0,,.00""万""" & Chr(10) & "%"
            End Select

            Selection.WrapText = True
            Selection.EntireRow.AutoFit
        Next
    Else
    
    End If

End Sub

'=======================
'
'  单 元 格 数 据 验 证
'
'=======================

Sub identityCard(control As IRibbonControl)
    
    Dim rngNo1 As String
    rngNo1 = Selection.Cells(1).address(0, 0)

    With Selection.Validation
        .Delete
        .Add Type:=xlValidateCustom, _
          AlertStyle:=xlValidAlertStop, _
          Operator:=xlBetween, _
          Formula1:="=OR(AND(LEN(" & rngNo1 & ")=15,ISNUMBER(--VALUE(" & rngNo1 & "))),AND(LEN(" & rngNo1 & ")=18,ISNUMBER(--LEFT(" & rngNo1 & ",17)),OR(ISNUMBER(--RIGHT(" & rngNo1 & ",1)),RIGHT(" & rngNo1 & ",1)=""X"")))"
        .ignoreBlank = True
        .InputTitle = "身份证号"
        .InputMessage = "18位字符或15位字符"
        .ErrorTitle = "录入身份证出错"
        .ErrorMessage = "您输入的可能不符合身份证规范，请检查"
        .IMEMode = xlIMEModeNoControl
        .ShowInput = True
        .ShowError = True
    End With

End Sub

' 手机号码验证
' 11位数字字符
' 可用两个空格或-分隔，不能单独存在
Sub phoneNumber(control As IRibbonControl)

    Dim rngNo1 As String
    rngNo1 = Selection.Cells(1).address(0, 0)

    With Selection.Validation
        .Delete
        .Add Type:=xlValidateCustom, AlertStyle:=xlValidAlertStop, Operator:= _
        xlBetween, Formula1:="=OR(AND(LEN(" & rngNo1 & ")=11,ISNUMBER(--VALUE(" & rngNo1 & "))),LEN(" & rngNo1 & ")=13)"
        .ignoreBlank = True
        .InputTitle = "手机号"
        .ErrorTitle = "手机号不合规范"
        .InputMessage = "11位字符可用两个空格或-分隔"
        .ErrorMessage = "您输入手机号不符合规范，请检查"
        .IMEMode = xlIMEModeNoControl
        .ShowInput = True
        .ShowError = True
    End With

End Sub

' 银行卡号验证
' 13~29位纯数字，中间不能添加空格等其他符号
Sub BankCard(control As IRibbonControl)
'-------- http://www.woshipm.com/pd/371041.html ------------------------

    Dim rngNo1 As String
    rngNo1 = Selection.Cells(1).address(0, 0)

    With Selection.Validation
        .Delete
        .Add Type:=xlValidateCustom, AlertStyle:=xlValidAlertStop, Operator:= _
        xlBetween, Formula1:="=AND(LEN(" & rngNo1 & ")>13 , LEN(" & rngNo1 & ")<19,ISNUMBER(--VALUE(" & rngNo1 & ")))"
        .ignoreBlank = True
        .InputTitle = "银行卡号"
        .ErrorTitle = "银行卡号不合规范"
        .InputMessage = "13~19位数字字符"
        .ErrorMessage = "您输入银行卡号不符合规范，请检查"
        .IMEMode = xlIMEModeNoControl
        .ShowInput = True
        .ShowError = True
    End With

End Sub

' 一级区划
' 紧随其后，会是二级区划和三级区划
Sub premierLevel(control As IRibbonControl)
    
    If Selection.Columns.Count > 1 Then
        MsgBox "请选择要设置一级区划的数据（单列）"

    Else
        Excel.Application.ScreenUpdating = False
        Dim selsht As Worksheet, selRng As Range
        
        '复制表格到当前工作簿
        On Error Resume Next
        Set selsht = ActiveSheet
        Set selRng = Selection
        
        Application.DisplayAlerts = False   '阻止是否替换当前表对话框
        If hasWorkSheet("中国所有区划名称表（勿删）") = False Then ThisWorkbook.Worksheets("中国所有区划名称表（勿删）").Copy before:=ActiveWorkbook.Worksheets(1)
        If hasWorkSheet("中国一级区划名称表（勿删）") = False Then ThisWorkbook.Worksheets("中国一级区划名称表（勿删）").Copy before:=ActiveWorkbook.Worksheets(1)
        If hasWorkSheet("中国二级区划名称表（勿删）") = False Then ThisWorkbook.Worksheets("中国二级区划名称表（勿删）").Copy before:=ActiveWorkbook.Worksheets(1)
        If hasWorkSheet("中国三级区划名称表（勿删）") = False Then ThisWorkbook.Worksheets("中国三级区划名称表（勿删）").Copy before:=ActiveWorkbook.Worksheets(1)
        
        Application.DisplayAlerts = True
        
        On Error GoTo 0

        '创建名称
        Application.DisplayAlerts = False   '阻止是否替换对话框
        Dim i As Long
        With ActiveWorkbook.Worksheets("中国二级区划名称表（勿删）")
            For i = 1 To .Cells(1, 1).End(xlToRight).Column
                .Cells(1, i).Resize(.Cells(1, i).End(xlDown).Row, 1).CreateNames Top:=True, Left:=False, Bottom:=False, Right:=False '添加到名称管理器
            Next
        End With
        With ActiveWorkbook.Worksheets("中国三级区划名称表（勿删）")
            For i = 1 To .Cells(1, 1).End(xlToRight).Column
                .Cells(1, i).Resize(.Cells(1, i).End(xlDown).Row, 1).CreateNames Top:=True, Left:=False, Bottom:=False, Right:=False '添加到名称管理器
            Next
        End With
        Application.DisplayAlerts = True
        
        ' 隐藏sheet，不在前端显示
        ActiveWorkbook.Worksheets("中国所有区划名称表（勿删）").Visible = xlVeryHidden
        ActiveWorkbook.Worksheets("中国一级区划名称表（勿删）").Visible = xlVeryHidden
        ActiveWorkbook.Worksheets("中国二级区划名称表（勿删）").Visible = xlVeryHidden
        ActiveWorkbook.Worksheets("中国三级区划名称表（勿删）").Visible = xlVeryHidden
        
        '清除错误值
        Dim n As Name
        For Each n In ActiveWorkbook.Names
            If InStr(n, "#REF!") Then n.Delete
        Next

        Dim pre1 As Variant, fm As String
        '返回r×1的二维数组
        pre1 = ActiveWorkbook.Worksheets("中国一级区划名称表（勿删）").Range(ActiveWorkbook.Worksheets("中国一级区划名称表（勿删）").[A1], ActiveWorkbook.Worksheets("中国一级区划名称表（勿删）").[A1].End(xlDown))

        For i = LBound(pre1) To UBound(pre1) - 1
            fm = fm & pre1(i, 1) & ","
        Next
        fm = fm & pre1(UBound(pre1), 1)
        
        selsht.Select
        With selRng.Validation
            .Delete
            .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _
            xlBetween, Formula1:=fm
            .ignoreBlank = True     ' 忽略空值
            .InCellDropdown = True  ' 提供下拉列表
            .InputTitle = "一级区划"
            .ErrorTitle = ""
            .InputMessage = ""
            .ErrorMessage = ""
            .IMEMode = xlIMEModeNoControl
            .ShowInput = True       ' 提示要输入什么信息
            .ShowError = False      ' 不提示错误信息
            .ignoreBlank = True
        End With
        
        If MsgBox("是否需要继续添加二级、三级区划？此操作可能会破坏单元格原始数据？", vbYesNo + vbInformation) = vbYes Then
            Dim arr1, arr2  ' 保存当前单元格数据
            arr1 = selRng.Value
            arr2 = selRng.Offset(0, 1).Value
            
            ' 因为限制，无法在单元格为空时使用 INDIRECT
            selRng.Value = "河北省"
            With selRng.Offset(0, 1).Validation
                .Delete
                .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _
                xlBetween, Formula1:="=INDIRECT(" & selRng.Cells(1).address(0, 0) & ")"
                .ignoreBlank = True     ' 忽略空值
                .InCellDropdown = True  ' 提供下拉列表
                .InputTitle = "二级区划"
                .ErrorTitle = ""
                .InputMessage = ""
                .ErrorMessage = ""
                .IMEMode = xlIMEModeNoControl
                .ShowInput = True       ' 提示要输入什么信息
                .ShowError = False      ' 不提示错误信息
                .ignoreBlank = True
            End With
            selRng.Offset(0, 1).Value = "承德市"
            With selRng.Offset(0, 2).Validation
                .Delete
                .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _
                xlBetween, Formula1:="=INDIRECT(" & selRng.Cells(1).address(0, 0) & "&""_""&" & selRng.Offset(0, 1).Cells(1).address(0, 0) & ")"
                .ignoreBlank = True     ' 忽略空值
                .InCellDropdown = True  ' 提供下拉列表
                .InputTitle = "三级区划"
                .ErrorTitle = ""
                .InputMessage = ""
                .ErrorMessage = ""
                .IMEMode = xlIMEModeNoControl
                .ShowInput = True       ' 提示要输入什么信息
                .ShowError = False      ' 不提示错误信息
                .ignoreBlank = True
            End With
            
            ' 恢复为初始化的信息
            selRng.Value = arr1
            selRng.Offset(0, 1).Value = arr2
            MsgBox "已完成一、二、三级区划设置！"

        Else
            MsgBox "已完成一级区划设置！"
        End If
        Application.ScreenUpdating = False
    End If

End Sub

'清除数据验证
Sub cleanVerification(control As IRibbonControl)
    Selection.Validation.Delete
End Sub

'========================
'
'  单 元 格 信 息 读 取
'
'========================

' 读取身份证信息
Sub readIdentityCard(control As IRibbonControl)
    IDCardForm.Show
End Sub

'========================
'
'    原 位 性 粘 贴
'
'========================

'原位粘贴为值和源格式()
Sub pasteValue(control As IRibbonControl)
'只需定位到要执行的区域即可

    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteAllUsingSourceTheme, Operation:=xlNone _
        , SkipBlanks:=False
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False
    Application.CutCopyMode = False

End Sub
'原位粘贴为显示的值()
Sub pasteDisplayValue(control As IRibbonControl)
'只需定位到要执行的区域即可

    Dim arr
    Set arr = Selection

    Dim rng As Range
    For Each rng In arr
        rng.Value = rng.text
        rng.NumberFormatLocal = "@"
    Next

End Sub
'粘贴行高列宽格式()
Sub pasteRowHColumnW(control As IRibbonControl)
'可跨sheet运行

    Dim rng1 As Range, rng2 As Range
    Dim i As Integer, j As Integer

    Set rng1 = Selection
    
    On Error Resume Next
    Set rng2 = Application.InputBox(" 该格式应用在哪个区域？", Type:=8)
    If Err.Number = 424 Then GoTo cancelSelect
    On Error GoTo 0
    
    If rng1.Cells.Count = 1 Then           '1→N 多次复制
        Dim rng As Range
        For Each rng In rng2
            rng.ColumnWidth = rng1.ColumnWidth
            rng.RowHeight = rng1.RowHeight
        Next
    ElseIf rng2.Cells.Count = 1 Then        'N→1 依次复制
        For i = 1 To rng1.Rows.Count
            For j = 1 To rng1.Columns.Count
                rng2.Offset(i - 1, j - 1).ColumnWidth = rng1.Cells(i, j).ColumnWidth
                rng2.Offset(i - 1, j - 1).RowHeight = rng1.Cells(i, j).RowHeight
            Next
        Next
    Else
        MsgBox "只能1→N或N→1，且N为连续区域"
    End If
    Exit Sub '提前结束，避免运行错误提示

cancelSelect:
    MsgBox ("已取消选择/输入，程序已结束"):
    Err.Clear
End Sub

Sub setRowHColumnW(control As IRibbonControl)
    CellsSizeForm.Show
End Sub


'========================
'
'    图 片 图 形 处 理
'
'========================
Sub selectPicture(control As IRibbonControl)
'    Dim rng As Range, nowRng As Range, sRng() As Range, i As Long
'    Set nowRng = Selection
'    For Each rng In nowRng
'        If rng.value Like "*:\*jpg" Then
'            sRng(i) = rng
'            i = i + 1
'        End If
'    Next
'    For i = LBound(sRng) To UBound(sRng)
'
'    Next

End Sub
'批量插入图片()
Sub insertPicture(control As IRibbonControl)

    If MsgBox("请确认已设置完整路径（D:\folder\abc.jpg）", vbOKCancel) = vbOK Then

        Dim rng1 As Range
        Set rng1 = Selection.Cells '存储选区

        Dim picdir As String
        picdir = InputBox("请输入要偏移的位置，以英文逗号分隔(下移y行,右移x列)")
        If picdir <> "" Then
            Dim errRng As Range, errNumber As Long  '存储发生错位的单元格
            
            Dim x As Double, y As Double
            x = Val(Split(picdir, ",")(0))
            If InStr(picdir, ",") <> 0 Then y = Val(Split(picdir, ",")(1))
            Dim rng As Range, rng2 As Range
            Dim shp As Shape
            For Each rng In rng1
                Set rng2 = rng.Offset(x, y)
                If rng.Value <> "" Then
                    On Error GoTo cannotFind
                    Set shp = ActiveSheet.Shapes.AddPicture(rng.Value, msoFalse, msoCTrue, rng2.MergeArea.Left, rng2.MergeArea.Top, rng2.MergeArea.width, rng2.MergeArea.Height) '可匹配合并单元格
                    shp.Placement = xlMoveAndSize '随单元格大小和位置改变
                End If
            Next
            If errNumber > 0 Then
                MsgBox "有" & errNumber & "个图片无法添加，已为您选中错误单元格"
                errRng.Select
            End If
        End If
        
    End If
    Exit Sub
cannotFind:
    If errNumber = 0 Then
        Set errRng = rng
    Else
        Set errRng = Union(errRng, rng) '加选区，方便之后选择后展示出来
    End If
    errNumber = errNumber + 1
    Resume Next
End Sub
' 批量导出图片
Sub exportPicture(control As IRibbonControl)

    If MsgBox("此功能导出的图片非原图，无法保存透明度信息" & Chr(10) & "请确保图片左上角所在单元格不共用，图片分辨率与你的屏幕分辨率有关，是否继续执行", vbOKCancel) <> vbOK Then Exit Sub
    
    Dim filePath
    With Application.FileDialog(msoFileDialogFolderPicker)
        .Title = "请选择要导出图片到哪个文件夹（此操作会覆盖重名文件）"
        If .Show Then
            filePath = .SelectedItems(1)
        Else
            MsgBox "未选择文件夹，操作已中止"
            Exit Sub
        End If
    End With
    If Right(filePath, 1) <> "\" Then filePath = filePath & "\"

    Dim offsetText As String, relRow As Long, relColumn As Long
    offsetText = InputBox("请输入图片名称来源（不要指定后缀名）所在单元格据图片左上角所有单元格的位置" & "如「0,1」表示图片右侧第一个单元格")

    If InStr(offsetText, ",") Then
        relRow = CLng(Split(offsetText, ",")(0))
        relColumn = CLng(Split(offsetText, ",")(1))
    Else
        MsgBox "录入出错，程序已退出"
        Exit Sub
    End If

    Application.ScreenUpdating = False
    With ActiveSheet
        Dim shp As Shape
        On Error Resume Next
        Dim h, w
        For Each shp In .Shapes
            With shp
                    '当前显示宽高
                    h = .Height
                    w = .width
                    '重置为原始图片大小
                    .ScaleHeight 1, msoCTrue
                    .ScaleWidth 1, msoCTrue
    
                    .CopyPicture
                    With ActiveSheet.ChartObjects.Add(0, 0, shp.width, shp.Height).Chart '在工作表中添加一个图表对象
                        .Parent.Select
                        .Paste
                        .export filePath & shp.TopLeftCell.Offset(relRow, relColumn).text & ".jpg", "jpg"
                        .Parent.Delete '删除工作表中添加的图表对象
                    End With
    
                    '重置为图片一开始的尺寸
                    .Height = h
                    .width = w
                End With

        Next
    End With

    Application.ScreenUpdating = True
    
    
    If InStr(Application.Path, "WPS") > 0 Then
        MsgBox "已保存到选定文件夹内！因WPS限制，请手动打开所在文件夹"
    Else
        MsgBox "已保存到选定文件夹内！"
        Shell "explorer.exe /n, /e, " & filePath, vbNormalFocus
    End If
    
End Sub

'清除所有图形()
Sub deleteShape(control As IRibbonControl)
    Dim msg As VbMsgBoxResult
    msg = MsgBox("请确定清除该sheet中仅图片（Y）还是所有图形（N）？", vbYesNoCancel)
    If msg <> vbCancel Then
        Dim shp As Shape
        For Each shp In ActiveSheet.Shapes
            If msg = vbYes Then
                If shp.Type = msoPicture Then shp.Delete
            Else
                shp.Delete
            End If
        Next
    Else
    End If
End Sub

' ================================= '
'                                   '
'          空单元格处理             '
'                                   '
' ================================= '

'定位到选区的空值单元格()
Sub locateNull(control As IRibbonControl)
    Selection.SpecialCells(xlCellTypeBlanks).Select
End Sub
' 删除空单元格所在行/列
Sub deleteBlankEntireRow(control As IRibbonControl)
    If MsgBox("此操作将删除空格单元格所在行，且无法撤销，请确认", vbOKCancel + vbExclamation) = vbOK Then
        Selection.SpecialCells(xlCellTypeBlanks).EntireRow.Delete
    End If
End Sub
Sub deleteBlankEntireColumn(control As IRibbonControl)
    If MsgBox("此操作将删除空格单元格所在列，且无法撤销，请确认", vbOKCancel + vbExclamation) = vbOK Then
        Selection.SpecialCells(xlCellTypeBlanks).EntireColumn.Delete
    End If
End Sub

'批量拆分合并单元格
Sub unmergeCells(control As IRibbonControl)

    Dim rng As Range
    Set rng = Selection '存储选区到内存中，否则后续在定位空白单元格时可能会重置选区
    With rng
        Dim 方向 As String
        If .Cells.Columns.Count = 1 Then
            方向 = "向下"
        ElseIf .Cells.Rows.Count = 1 Then
            方向 = "向右"
        Else
            Dim msg
            msg = MsgBox("合并单元格之前是向下合并的么？选否则为向右合并的", vbYesNoCancel)
            If msg = vbYes Then
                方向 = "向下"
            ElseIf msg = vbNo Then
                方向 = "向右"
            End If
        End If

        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlCenter
        .mergeCells = False
        Dim nf As Variant
        nf = .NumberFormat      '可能为Null
        .NumberFormatLocal = "G/通用格式"           ' 需要转换为常规格式才可以使用公式，文本格式的内容无法使用公式
        
        On Error Resume Next        '防止找不到空单元格报错
        If 方向 = "向下" Then
            .SpecialCells(xlCellTypeBlanks).FormulaR1C1 = "=R[-1]C"
        ElseIf 方向 = "向右" Then
            .SpecialCells(xlCellTypeBlanks).FormulaR1C1 = "=RC[-1]"
        End If
        
        .Copy
        .PasteSpecial Paste:=xlPasteAllUsingSourceTheme, Operation:=xlNone _
        , SkipBlanks:=False, Transpose:=False
        .PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False

        .NumberFormat = nf          ' 将之前的数字格式粘贴过来
        
    End With

End Sub

' ========================== '
'                            '
'     单元格合并/拆分        '
'                            '
' ========================== '

'批量合并单元格
Sub mergeCells(control As IRibbonControl)

    If InStr(Selection.address(0, 0), ":") Then         '如果是多个单元格
        Dim 方向 As String
        Dim rng As Range
        Set rng = Selection
        Dim rs As Long, cs As Long
        rs = rng.Cells.Rows.Count
        cs = rng.Cells.Columns.Count

        '判断合并方向
        If cs = 1 Then             ' 只有一列数据时
            方向 = "向下"
        ElseIf rs = 1 Then       ' 只有一行数据时
            方向 = "向右"
        Else                            ' 多行多列数据时，由用户手动选择
            Dim msg
            msg = MsgBox("是否向下合并单元格？否则向右合并", vbYesNoCancel)
            If msg = vbYes Then
                方向 = "向下"
            ElseIf msg = vbNo Then
                方向 = "向右"
            End If
        End If

        ' 此时必会执行合并单元格操作
        Application.DisplayAlerts = False
        Dim r As Long, c As Long        ' 初始单元格的坐标
        With Range(Split(rng.address(0, 0), ":")(0))
            r = .Row
            c = .Column
        End With

        Dim Lastval As Variant, nextVal As Variant
        Dim m As Long, n As Long
        Dim i As Long, j As Long

        If 方向 = "向下" Then

            For j = 0 To cs - 1

                n = 0
                Do While n < rs
                    m = n: n = m + 1        ' 从上一个不同值的单元格坐标开始这一次的判断
                    Lastval = Cells(r + m, c + j).Value
                    nextVal = Cells(r + n, c + j).Value
                    Do While Lastval = nextVal And n < rs
                        n = n + 1
                        nextVal = Cells(r + n, c + j).Value
                    Loop

                    ' 合并及居中两者之间的所有单元格
                    With Range(Cells(r + m, c + j), Cells(r + n - 1, c + j))
                        .HorizontalAlignment = xlCenter
                        .VerticalAlignment = xlCenter
                        .mergeCells = True
                    End With

                Loop
            Next

        ElseIf 方向 = "向右" Then

            For i = 0 To rs - 1
                n = 0

                Do While n < cs
                    m = n: n = m + 1
                    Lastval = Cells(r + i, c + m).Value
                    nextVal = Cells(r + i, c + n).Value
                    Do While Lastval = nextVal And n < cs
                        n = n + 1
                        nextVal = Cells(r + i, c + n).Value
                    Loop

                    With Range(Cells(r + i, c + m), Cells(r + i, c + n - 1))
                        .HorizontalAlignment = xlCenter
                        .VerticalAlignment = xlCenter
                        .mergeCells = True
                    End With

                Loop
            Next

        End If
        Application.DisplayAlerts = True
    End If

End Sub


' 会自动清除非首个单元格
' 所有内容以文本格式写在首个单元格内
Sub mergeCellsContent(control As IRibbonControl)
    With Selection
        Dim v As Variant
        v = .Cells(1).Value
        Dim i As Long
        If .Count > 1 Then
            For i = 2 To .Cells.Count
                v = v & .Cells(i).Value
                .Cells(i).ClearContents ' 可以使用定位空单元格继续下一步操作
            Next
        End If
        .Cells(1).Value = v
    End With
End Sub


'转换行列
Sub convertRowsColumns(control As IRibbonControl)

    Dim arr, cot As Long

    arr = Selection.Cells   '将数据存放在数组中
    cot = Selection.Cells.Count
    On Error GoTo 100
    
    Dim rng As Range
    Set rng = Application.InputBox("转置后的单元格放在区域的第一个单元格", Type:=8)

    Dim liehang As String

    liehang = InputBox("请输入需要转换的列数或行数其中之一，以"",""分隔（lie,[hang]）")

    Dim lie As Double, hang As Double
    lie = Val(Split(liehang, ",")(0))
    If InStr(liehang, ",") <> 0 Then hang = Val(Split(liehang, ",")(1))
    If hang = 0 Then hang = IIf(cot / lie = Int(cot / lie), cot / lie, Int(cot / lie) + 1)              '如果没写行数，自动计算
    If lie = 0 Then lie = IIf(cot / hang = Int(cot / hang), cot / hang, Int(cot / hang) + 1)     '如果没写列数

    Dim zixing As String
    zixing = InputBox("请选择需要转换前与转换后的形式，" & _
        Chr(13) & "  1：Z→N 横转竖" & Chr(9) & "  2：N→Z 竖转横" & _
        Chr(13) & "  3：Z→Z 横转横" & Chr(9) & "  4：N→N 竖转竖", "转换形式")

    Dim n As Long, m As Long, i As Long, j As Long
    m = 0: n = 0
    Application.ScreenUpdating = False

    If zixing = "1" Or zixing = "3" Then        '起始编号为先横后竖
        For i = LBound(arr) To UBound(arr)
            For j = LBound(arr, 2) To UBound(arr, 2)
                rng.Offset(m, n) = arr(i, j)
                
                If zixing = "1" Or zixing = "4" Then    '结束编号为先竖后横
                    If m < hang - 1 Then
                        m = m + 1
                    Else
                        n = n + 1
                        m = 0
                    End If
    
                ElseIf zixing = "2" Or zixing = "3" Then
                    If n < lie - 1 Then
                        n = n + 1
                    Else
                        m = m + 1
                        n = 0
                    End If
                End If
            Next
        Next
        
    ElseIf zixing = "2" Or zixing = "4" Then

        For i = LBound(arr, 2) To UBound(arr, 2)
            For j = LBound(arr) To UBound(arr)
                rng.Offset(m, n) = arr(j, i)
                If zixing = "1" Or zixing = "4" Then
                    If m < hang - 1 Then
                        m = m + 1
                    Else
                        n = n + 1
                        m = 0
                    End If
    
                ElseIf zixing = "2" Or zixing = "3" Then
                    If n < lie - 1 Then
                        n = n + 1
                    Else
                        m = m + 1
                        n = 0
                    End If
                End If
            Next
        Next
    End If

    Application.ScreenUpdating = False
    Exit Sub
    
100:
    MsgBox ("已取消选择/输入，程序已结束"):

End Sub

' 删除重复值
Sub removeDuplicates(control As IRibbonControl)

    Dim dic As Object
    Set dic = CreateObject("Scripting.Dictionary")
    
    Dim arr
    arr = Selection
    Dim i As Long, j As Long
    For i = 1 To UBound(arr, 1)
        For j = 1 To UBound(arr, 2)
            If dic.Exists(arr(i, j)) Then
                arr(i, j) = ""
            Else
                dic(arr(i, j)) = ""
            End If
        Next
    Next

    Selection = arr
    
End Sub

' 批量添加超链接
Sub addHyperLinks(control As IRibbonControl)
    On Error GoTo errLine
    Dim sel As Range
    Set sel = Intersect(Selection, ActiveSheet.UsedRange)   ' 避免选中未空白单元格而出错
    If sel.Columns.Count > 1 And sel.Rows.Count > 1 Then
        MsgBox "请先选择单列/行的单元格"
    ElseIf sel.Columns.Count = 1 And sel.Rows.Count = 1 Then
        MsgBox "此操作需要选中至少两个单元格"
    Else
        Dim inpRng As Range
        Set inpRng = Excel.Application.InputBox("请选中超链接地址的任一单元格，程序会自动判断", Type:=8)
        Dim offCol As Long, offRow As Long
        If sel.Columns.Count = 1 Then
            offCol = inpRng.Column - sel.Column
            offRow = 0
        ElseIf sel.Rows.Count = 1 Then
            offRow = inpRng.Row - sel.Row
            offCol = 0
        End If
        Dim rng As Range
        For Each rng In sel
            ActiveSheet.Hyperlinks.Add Anchor:=rng, address:=rng.Offset(offRow, offCol).Value
        Next
    End If

    Exit Sub
    
errLine:
    MsgBox "程序已终止！请按如下步骤重试：" & Chr(13) & _
      "  1.选中要展示文字所在的多个单元格（单列/行）" & Chr(13) & _
      "  2.点击此功能按钮" & Chr(13) & _
      "  3.选中超链接地址所在任一单元格"
End Sub

'========================
'
'    工 作 表 处 理
'
'========================

'取消全部sheet隐藏()
Sub showAllWorksheets(control As IRibbonControl)
    Dim sht As Worksheet
    For Each sht In Worksheets
        sht.Visible = xlSheetVisible
    Next
End Sub

'选择除1外所有可见sheet()
Sub selectWorksheets(control As IRibbonControl)
    
    'Worksheets.Select：不可取消sheets(1)的选中状态
    
    Dim arr() '存储已显示的工作表名
    '不选择第一个sheet
    Dim k As Long
    k = 0
    Dim sht As Worksheet
    For Each sht In Worksheets
        If sht.Visible = True Then
           k = k + 1
           ReDim Preserve arr(1 To k) '重新定义数组大小，但保留之前的值
           arr(k) = sht.Name
        End If
    Next
    
    If UBound(arr) - LBound(arr) > 0 Then
        Sheets(arr(LBound(arr) + 1)).Select '先选中可见的第二个sheet，也就取消了第一个sheet的选中状态
        For Each sht In Worksheets
            Dim flag As Byte
            flag = False    '假设没有重复值
            Dim i As Long
            For i = LBound(arr) + 1 To UBound(arr) '排除第一张可见sheet
                If sht.Name = arr(i) Then
                    flag = True
                    Exit For
                End If
            Next
            If flag = True Then sht.Select Replace:=False   '加选，不替换之前的选择
        Next
    Else
        MsgBox "仅有一张工作表可见"
    End If

End Sub

' 删除未选中的可见工作表
Sub deleteNotSelectedWorksheets(control As IRibbonControl)
    
    If MsgBox("此操作会永久删除所有其他已显示的sheet，请确定", vbOKCancel + vbExclamation) = vbOK Then
    
        Application.ScreenUpdating = False

        Dim arr() As String
        
        Dim cou As Long
        cou = ActiveWindow.SelectedSheets.Count
        ReDim arr(1 To cou)
        
        Dim i As Long
        i = LBound(arr)
        Dim sht As Worksheet
        For Each sht In ActiveWindow.SelectedSheets '将被选择的sheet名称存储在数组中
            arr(i) = sht.Name
            i = i + 1
        Next
    
        For Each sht In Sheets
            If sht.Visible = xlSheetVisible Then '如果为显示状态
                Dim flag As Byte
                flag = False    '假设表格没有被选中，可删除
                For i = LBound(arr) To UBound(arr)
                    If sht.Name = arr(i) Then
                        flag = True '代表已经被选中，不能删除
                        Exit For
                    End If
                Next
                If flag = False Then
                    Excel.Application.DisplayAlerts = False '忽略弹窗警告
                    On Error Resume Next
                    sht.Delete
                    Excel.Application.DisplayAlerts = True
                End If
            End If
        Next
        
        Application.ScreenUpdating = False
    End If

End Sub

' 去除非法字符
Private Function RemoveIllegalCharts(ByVal str, illCharts)
    Dim o As String
    o = str
    If IsArray(illCharts) Then
        Dim i As Long
        For i = LBound(illCharts) To UBound(illCharts)
            o = Replace(o, illCharts(i), "")
        Next
    Else
        o = Replace(o, illCharts, "")
    End If
    RemoveIllegalCharts = o
End Function

' 将字符串型数组填充到单元格中
Private Sub arrStrToRange(arrStr, startRng As Range)
    Dim splitRow, arr
    splitRow = Split(arrStr, Chr(13))
    Dim i As Long
    For i = LBound(splitRow) To UBound(splitRow)
        Dim splitCol, j As Long
        splitCol = Split(splitRow(i), Chr(9))
        For j = LBound(splitCol) To UBound(splitCol)
            startRng.Offset(i - LBound(splitRow), j - LBound(splitCol)).Value = splitCol(j)
        Next
    Next
End Sub


'多工作簿的表合并在当前文件中()
Sub workbooksMerge(control As IRibbonControl)
    
    Dim activeShtName As String
    activeShtName = ActiveSheet.Name  ' 获取当前激活的工作表名，方面后续再次激活
    Dim actWb As Workbook
    Set actWb = ActiveWorkbook

    On Error GoTo endSubLine '防止用户点击取消出错
    Dim userSelectFilesPathArr()           ' 存储用户选择的工作簿文件
    userSelectFilesPathArr = Excel.Application.GetOpenFilename("Excel数据文件, *.xls*;*.xla*;*.xlt*", Title:="请选择要合并的文件", MultiSelect:=True)
    On Error GoTo 0 ' 恢复错误捕获
    
    Application.ScreenUpdating = False
    Dim 非法字符 As Variant
    非法字符 = Array("/", "\", "[", "]", "!", "?", "*", ":")
    Const 浅北对应表 As String = "浅北对应表"
    
    Dim arrStr As String ' 使用文本保存的数组，使用Chr(9)列分隔，使用chr(13)行分隔
    
    Dim i As Long, wb As Workbook
    Dim flag As Boolean: flag = False    ' 是否需要创建对应表
    For i = LBound(userSelectFilesPathArr) To UBound(userSelectFilesPathArr)
        Application.StatusBar = "正在导入 " & i & " / " & UBound(userSelectFilesPathArr)
        Set wb = Workbooks.Open(userSelectFilesPathArr(i))
        Dim sht As Worksheet
        For Each sht In wb.Worksheets
            If sht.Visible = xlSheetVisible Then
                sht.Copy after:=actWb.Worksheets(actWb.Worksheets.Count)    ' 复制在最后的位置，没有返回值

                Dim wbName As String, shtName As String, newWbName As String, newShtName As String, nowShtName As String
                wbName = Left(wb.Name, InStrRev(wb.Name, ".") - 1)
                shtName = sht.Name
                ' 不允许出现非法字符
                newWbName = RemoveIllegalCharts(wbName, 非法字符)
                newShtName = RemoveIllegalCharts(shtName, 非法字符)
                If newWbName <> wbName Or newShtName <> shtName Then
                    flag = True
                End If
                ' 不允许超过31个字符
                If Len(newWbName) + Len(newShtName) < 31 Then
                    nowShtName = newWbName & "_" & newShtName
                Else
                    If Len(newShtName) < 30 Then
                        nowShtName = Left(newWbName, 30 - Len(newShtName)) & "_" & newShtName
                    End If
                    flag = True
                End If
                On Error Resume Next
                actWb.Sheets(actWb.Worksheets.Count).Name = nowShtName
                
                arrStr = arrStr & wbName & Chr(9) & shtName & Chr(9) & actWb.Sheets(actWb.Worksheets.Count).Name & Chr(13)
            End If
        Next
        wb.Close
    Next

    Application.ScreenUpdating = True
    Application.StatusBar = ""

    If flag Then
        MsgBox "已完成合并，但部分名称有变更，已为你打开变更列表"
        If hasWorkSheet(浅北对应表) = False Then
            ActiveWorkbook.Worksheets.Add.Name = 浅北对应表
        End If
        With ActiveWorkbook.Worksheets(浅北对应表)
            .Cells.Clear
            .Range("A1") = "原工作簿名称"
            .Range("B1") = "原工作表名称"
            .Range("C1") = "现工作簿名称"
            ' 将数组写会到单元格中
            Call arrStrToRange(arrStr, .Range("A2"))
            .Activate
            Exit Sub
        End With
    End If

endSubLine:
    actWb.Worksheets(activeShtName).Activate
End Sub


Sub worksheetsMerge(control As IRibbonControl)

    If MsgBox("请确定已选择要合并的sheet！", vbOKCancel) <> vbOK Then
        Exit Sub
    End If
    
    ' 1. 将选中工作表存入数组，防止新建/切换工作表时发生变化
    Dim selectedSheetsNameArr() As String
    ReDim selectedSheetsNameArr(1 To ActiveWindow.SelectedSheets.Count) '选中的工作表名称

    Dim sht As Variant  'Chart、WorkSheet
    Dim i As Long: i = 0
    For Each sht In ActiveWindow.SelectedSheets
        If sht.Type = xlWorksheet Then  ' 仅存放 Worksheet
            i = i + 1
            selectedSheetsNameArr(i) = sht.Name
        End If
    Next
    ReDim Preserve selectedSheetsNameArr(1 To i)
    

    ' 2. 让用户选择数据所在的最左上位置
    ' 2.1 重置选择工作表，兼容WPS
    Dim activeSht As Worksheet
    Set activeSht = Worksheets(selectedSheetsNameArr(1))
    activeSht.Select Replace:=True
    ' 2.2 用户选择单元格
    On Error Resume Next '防止点击取消发生错误
    Dim dataStartRange As Range
    Set dataStartRange = Excel.Application.InputBox(" 请选择非标题的数据区域的最左上单元格", Type:=8)
    If dataStartRange Is Nothing Then
        MsgBox ("您未选择单元格，程序已结束")
        Exit Sub
    End If
    On Error GoTo 0
    
    Excel.Application.ScreenUpdating = False
    
    ' 2.3 获取用户选择单元格的各种信息
    Dim dataStartRangeAddress As String, dataStartRangeRow As Long, dataStartRangeColumn As Long
    dataStartRangeAddress = dataStartRange.address(0, 0)
    dataStartRangeRow = dataStartRange.Row
    dataStartRangeColumn = dataStartRange.Column

    ' 3. 创建用于合并的工作表
    Dim mergeSht As Worksheet
    Set mergeSht = Worksheets.Add(before:=Sheets(1), Count:=1) '需添加count，因为默认会添加你选择sheet的数量
    
    ' 4. 循环遍历，复制粘贴
    Dim mergeShtStartRangeRow As Long
    mergeShtStartRangeRow = dataStartRangeRow   '设置起始行号
    For i = LBound(selectedSheetsNameArr) To UBound(selectedSheetsNameArr)
        Application.StatusBar = "正在合并…… " & i & " / " & UBound(selectedSheetsNameArr)
        With Worksheets(selectedSheetsNameArr(i))

            ' 如果有数据
            If .Range(dataStartRangeAddress).CurrentRegion.Cells.Count > 1 Then
                ' 标题范围
                Dim theadRange As Range
                Set theadRange = Intersect(.Rows("1:" & dataStartRangeRow - 1), .Range(dataStartRangeAddress).CurrentRegion)
                If theadRange Is Nothing Then    ' 如果标题部分为空
                    Exit For    ' 不再继续判断+读取数据范围
                Else
                    If i = 1 Then   ' 只复制一次，其他的不复制
                        theadRange.Copy mergeSht.Range(theadRange.Cells(1).address(0, 0))
                        mergeSht.Columns("A:A").Insert Shift:=xlToRight '插入一列用户填写工作表
                        With mergeSht.Range(theadRange.Cells(1).address(0, 0)).Resize(theadRange.Rows.Count, 1)
                            .Merge
                            .Value = "工作表名称"
                        End With
                    End If
                End If

                ' 数据范围
                Dim tbodyRange As Range
                Set tbodyRange = Intersect(.Range(.Range(dataStartRangeAddress), .Cells(.Cells.Rows.Count, .Cells.Columns.Count)), .Range(dataStartRangeAddress).CurrentRegion)
                tbodyRange.Copy mergeSht.Cells(mergeShtStartRangeRow, dataStartRangeColumn + 1)
                mergeSht.Range(mergeSht.Cells(mergeShtStartRangeRow, dataStartRangeColumn), mergeSht.Cells(mergeShtStartRangeRow + tbodyRange.Rows.Count - 1, dataStartRangeColumn)).Value = .Name
                mergeShtStartRangeRow = mergeShtStartRangeRow + tbodyRange.Rows.Count
            End If
        End With
    Next

    Excel.Application.ScreenUpdating = True
    Application.StatusBar = ""
    mergeSht.Select
    MsgBox "已完成工作表的合并！"

End Sub

'按列拆分表格()
Sub createWorksheetsByColumn(control As IRibbonControl)

    Dim 非法字符 As Variant
    非法字符 = Array("/", "\", "[", "]", "!", "?", "*", ":")
    Const 浅北对应表 As String = "浅北对应表"
    
    If MsgBox("请确定以下内容：" & Chr(10) _
    & "1. 该Sheet为清单式表格(首行为标题行，且为Row(1)" & Chr(10) _
    & "2. 已选择拆分的依据列中的某单元格", vbOKCancel) = vbOK Then
        Const blackShtName As String = "空白字段表"
        
        Excel.Application.DisplayAlerts = False
        Excel.Application.ScreenUpdating = False
        Dim actsht As Worksheet
        Set actsht = ActiveSheet '存储当前活动的sheet名称

        With actsht
            
            ' 1. 获取选中单元格信息
            Dim activeCellColnum As Long
            activeCellColnum = ActiveCell.Column
            Dim actshtCurrentRegionAddress As String
            actshtCurrentRegionAddress = actsht.Range("A1").CurrentRegion.address

            ' 2. 禁用筛选，防止出错
            .Range("A1").CurrentRegion.AutoFilter

            ' 3. 获取不重复的列数据作为表名
            Dim arr
            arr = Intersect(actsht.Range("A1").CurrentRegion, actsht.Columns(activeCellColnum)).Value
 
            Dim dict As Object
            Set dict = CreateObject("scripting.dictionary")
            Dim i As Long
            For i = LBound(arr) To UBound(arr)
                If arr(i, 1) <> .Cells(1, activeCellColnum).Value Then
                    dict(arr(i, 1)) = ""
                End If
            Next

            ' 4. 遍历复制粘贴
            Dim flag As Boolean: flag = False   ' 默认不需要创建对应表
            Dim arrStr As String
            Dim k As Variant
            For Each k In dict.keys

                Dim tempShtName As String
                If k <> "" Then
                    tempShtName = k & ""
                Else
                    tempShtName = blackShtName
                End If
                
                Dim sht As Worksheet
                '按名称新建表格于最后
                ' 如果存在则让用户判断是否删除
                If hasWorkSheet(tempShtName) Then
                    Excel.Application.ScreenUpdating = True
                    Worksheets(tempShtName).Select
                    Dim msg As Long
                    msg = MsgBox("已有相同名称的工作表「" & tempShtName & "」，是否删除后重试？" & Chr(13) & "是(Y)：删除  否(N)：新建工作表  取消：跳过", vbYesNoCancel)
                    If msg = vbYes Then
                        Worksheets(tempShtName).Delete
                        Set sht = Worksheets.Add(after:=Sheets(Sheets.Count))
                        sht.Name = tempShtName
                    ElseIf msg = vbNo Then
                        Set sht = Worksheets.Add(after:=Sheets(Sheets.Count))
                        sht.Name = tempShtName & "_副本"
                    Else
                        GoTo gotoNext
                    End If
                    Excel.Application.ScreenUpdating = False
                Else    ' 否则创建新工作表
                    Set sht = Worksheets.Add(after:=Sheets(Sheets.Count))
                    Dim newTempShtName As String
                    newTempShtName = RemoveIllegalCharts(tempShtName, 非法字符)
                    If newTempShtName <> tempShtName Then
                        flag = True
                    ElseIf Len(newTempShtName) > 31 Then
                        flag = True
                        newTempShtName = Left(newTempShtName, 31)
                    Else
                    End If
                    sht.Name = newTempShtName
                    arrStr = arrStr & tempShtName & Chr(9) & newTempShtName & Chr(13)
                End If
                
                '复制对应内容到指定表格
                actsht.Range(actshtCurrentRegionAddress).AutoFilter Field:=activeCellColnum, Criteria1:=k    '启动筛选
                ' k & ""避免条件为数字，干扰选择
                actsht.Range(actshtCurrentRegionAddress).Copy sht.[A1]
gotoNext:
            Next

            '取消筛选
            .Select
            .Range("A1").CurrentRegion.AutoFilter

        End With
        
        Excel.Application.ScreenUpdating = False
        Excel.Application.DisplayAlerts = True
        
        
        
        If flag Then
            MsgBox "已拆分完成，但部分工作表名称有变化，已为您整理好", vbOKOnly
            If hasWorkSheet(浅北对应表) = False Then
                ActiveWorkbook.Worksheets.Add.Name = 浅北对应表
            End If
            With ActiveWorkbook.Worksheets(浅北对应表)
                .Cells.Clear
                .Range("A1") = "工作表理想名称"
                .Range("B1") = "工作表现实名称"
                ' 将数组写会到单元格中
                Call arrStrToRange(arrStr, .Range("A2"))
                .Activate
                Exit Sub
            End With
            
        Else
            MsgBox "拆分完成"
        End If
        
    End If

End Sub

'活动表另存为工作簿文件()
Sub toXlsx(control As IRibbonControl)

    Dim sht As Worksheet, filePath As String
    With Application.FileDialog(msoFileDialogFolderPicker)
        .Title = "请选择要保存到哪个文件夹（此操作会覆盖重名文件）"
        .AllowMultiSelect = False   '禁止多选
        If .Show Then
            filePath = .SelectedItems(1)
        Else
            MsgBox "未选择文件夹，操作已中止"
            Exit Sub
        End If
    End With
    If Right(filePath, 1) <> "\" Then filePath = filePath & "\"

    Dim wbFileFormat As Variant
    wbFileFormat = ActiveWorkbook.FileFormat    '获取当前文件的类型：xlxm、xls、xlsx……

    '关闭屏幕更新，隐藏文件保存的过程
    Application.ScreenUpdating = False
    Application.DisplayAlerts = False  '有重名直接文件覆盖
    For Each sht In ActiveWindow.SelectedSheets
        sht.Copy '该方法会直接复制到新建的工作簿中，即新工作簿文件为之后激活的窗口
        On Error Resume Next
        ActiveWorkbook.SaveAs FileName:=filePath & sht.Name, FileFormat:=wbFileFormat
        ActiveWorkbook.Close True
    Next
    Application.DisplayAlerts = True
    Application.ScreenUpdating = True
    
    If InStr(Application.Path, "WPS") > 0 Then
        MsgBox "处理完成！因WPS限制，请手动打开所在文件夹"
    Else
        MsgBox "处理完成！稍后将打开所在文件夹"
        '打开文件浏览器，默认最小化窗口改为正常且获取焦点
        Shell "explorer.exe /n, /e, " & filePath, vbNormalFocus
    End If

End Sub

'根据选区创建工作表
Sub createWorksheetsByRange(control As IRibbonControl)
    
    Dim rng As Range
    Dim cou, flag As Byte
    Dim i As Integer
    Dim actsht, actsel
    actsht = ActiveSheet.Name '保存当前活动sheet
    actsel = Selection.Cells.Count
    
    For Each rng In Selection
        flag = 0
        For i = 1 To Sheets.Count
            If Sheets(i).Name = (rng.Value & "") Or rng.Value & "" = "" Then
                flag = 1
                Exit For
            End If
        Next
        If flag = 0 Then
            Sheets.Add(after:=Sheets(Sheets.Count)).Name = rng.Value
        Else
            cou = cou + 1 '创建失败数目
        End If

    Next
    If cou <> 0 Then
        MsgBox "有 " & cou & " / " & actsel & " Sheet因名称重复/空值创建失败"
    Else
        MsgBox actsel & " 张 Sheet表创建成功"
    End If
    Sheets(actsht).Select

End Sub

'生成目录
Sub createDirectory(control As IRibbonControl)

    If MsgBox("请确认已选择需要生成目录的Sheet", vbOKCancel) = vbOK Then
        Dim shtarr(), cou As Long, i As Long, sht As Worksheet
        cou = ActiveWindow.SelectedSheets.Count
        ReDim shtarr(1 To cou)  '将所选sheet存入数组备用
        i = 1   '设置从1开始存储
        For Each sht In ActiveWindow.SelectedSheets
            Set shtarr(i) = sht
            i = i + 1
        Next

        On Error GoTo cancelChoose '防止点击取消发生错误
        Dim texttype As VbMsgBoxResult
        texttype = MsgBox("请选择要展示的文字，Yes为Sheet表名，No为单元格文字内容，Cancel为""Sheet1_B2""", vbYesNoCancel)
        Dim rng As Range
        Set rng = Application.InputBox("请选择你要链接到哪个单元格", Type:=8)
        If Err.Number = 13 Then GoTo cancelChoose    '如果点击了取消

        Dim textvalue As String, rngaddress As String  '显示的文字
        rngaddress = rng.address(0, 0)  '获取相对地址如"B2"
        Dim mulusht As Worksheet
        Set mulusht = Sheets.Add(before:=Sheets(1)) '   生成新sheet以存放目录
        With mulusht
            On Error Resume Next
            .Name = "浅北表格自动生成目录"
            
            With .Range("C3")
                .Value = "目录"
                .HorizontalAlignment = xlRight
                .VerticalAlignment = xlBottom
                With .Font
                    .Name = "Microsoft YaHei UI"
                    .Size = 20
                End With
            End With
            Range("D3").Value = "COUTENTS"
            
            '目录下方绿色横线
            With .Range("C3:D3")
                .Borders(xlDiagonalDown).LineStyle = xlNone
                .Borders(xlDiagonalUp).LineStyle = xlNone
                .Borders(xlEdgeLeft).LineStyle = xlNone
                .Borders(xlEdgeTop).LineStyle = xlNone
                With .Borders(xlEdgeBottom)
                    .LineStyle = xlContinuous
                    .ThemeColor = 10
                    .TintAndShade = -0.249977111117893
                    .Weight = xlThick
                End With
            End With
            '调整列宽
            Columns("A:A").ColumnWidth = 2
            Columns("B:B").ColumnWidth = 2
            Columns("C:C").ColumnWidth = 7.4
            Columns("D:D").ColumnWidth = 24
            Columns("E:E").ColumnWidth = 2
            Columns("F:F").ColumnWidth = 2

            For i = LBound(shtarr) To UBound(shtarr)
                Dim textdis As String
                Select Case texttype
                Case 6, vbYes '   YES
                    textdis = shtarr(i).Name
                Case 7, vbNo 'NO
                    textdis = shtarr(i).Range(rngaddress).Value
                Case 2, vbCancel 'CANCEL
                    textdis = shtarr(i).Name & "_" & shtarr(i).Range(rngaddress).Value
                End Select

                .Hyperlinks.Add Anchor:=Range("C" & i + 4), address:="", SubAddress:= _
                    shtarr(i).Name & "!" & rngaddress, ScreenTip:="由浅北表格助手创建", TextToDisplay:=textdis

               Range("B" & (i + 4) & ":E" & (i + 4)).RowHeight = 26.2

            Next
            
            '隐藏未使用单元格
            .Range(.Range("B" & (i + 6)).EntireRow, .Range("B" & (i + 6)).EntireRow.End(xlDown)).EntireRow.Hidden = True
            .Range(Columns("G:G"), .Columns("G:G").End(xlToRight)).EntireColumn.Hidden = True
            '隐藏编辑栏、网格线及行列号
            Application.DisplayFormulaBar = False
            ActiveWindow.DisplayGridlines = False
            ActiveWindow.DisplayHeadings = False
            
            '具体目录所在区域
            With Range([c5], [c5].End(xlDown))
                With .Font
                    .Name = "微软雅黑"
                    .Size = 11
                    .ThemeColor = xlThemeColorDark2
                    .TintAndShade = -0.749992370372631
                    .Underline = xlUnderlineStyleNone
                End With
                .HorizontalAlignment = xlLeft
                .VerticalAlignment = xlCenter
            End With
            
            '整个的背景色
            With .Range(["b2"], Range("E" & (i + 4))).Interior
                .Pattern = xlSolid
                .Color = 16119285
            End With

            .Activate   '激活到该工作表
        End With
    Else
        GoTo cancelChoose
    End If
    Exit Sub

cancelChoose:
    MsgBox ("已取消操作")
End Sub
Sub sortSheet(control As IRibbonControl)

    ' 尝试删除原有的排序工作表
    On Error Resume Next
    Excel.Application.DisplayAlerts = False
    Worksheets("浅北工作表排序").Delete
    Excel.Application.DisplayAlerts = True
    On Error GoTo 0
    
    ' 获取当前工作簿所有工作表的名称，包括隐藏的工作表
    Dim shtNameList() As String
    ReDim shtNameList(1 To ActiveWorkbook.Worksheets.Count, 1 To 1)
    
    Dim sht As Worksheet
    Dim i As Long: i = 1
    For Each sht In ActiveWorkbook.Worksheets
        shtNameList(i, 1) = sht.Name
        i = i + 1
    Next


    ' 新建工作表并写入工作表名称
    Dim listSht As Worksheet
    Set listSht = Worksheets.Add
    
    listSht.Name = "浅北工作表排序" ' 设置唯一名称，之后容易删除
    listSht.Select
    listSht.Range("A1").Value = "工作表名称"
    listSht.Range("A2").Resize(UBound(shtNameList), 1).Value = shtNameList

    ' 创建按钮
    With listSht.Buttons.Add(180, 24, 100, 40)
        .OnAction = "startSortSheet"
        .Characters.text = "开始排序"
        With .Characters(Start:=1, Length:=4).Font
            .Name = "微软雅黑"
            .FontStyle = "常规"
            .Size = 11
            .Strikethrough = False
            .Superscript = False
            .Subscript = False
            .OutlineFont = False
            .Shadow = False
            .Underline = xlUnderlineStyleNone
            .ColorIndex = 1
        End With
    End With
    
    ' 添加排序筛选按钮
    listSht.Range("A1").CurrentRegion.AutoFilter
End Sub
' 点击按钮后执行的程序
Sub startSortSheet()
    Dim arr
    Excel.Application.ScreenUpdating = False
    With Worksheets("浅北工作表排序")
        ' 获取排序后的工作表
        arr = .Range(.Range("A2"), .Range("A2").End(xlDown)).Value
        On Error Resume Next
        Dim i As Long
        For i = LBound(arr) To UBound(arr)
            Worksheets(arr(i, 1)).Move before:=Worksheets(i)
        Next

        ' 删除此工作表
        Excel.Application.DisplayAlerts = False
        .Delete
        Excel.Application.DisplayAlerts = True

    End With
    Excel.Application.ScreenUpdating = True
End Sub


'========================
'
'     邮 件 合 并
'
'========================
Sub dataSources(control As IRibbonControl)
    If hasWorkSheet("浅北来源表") = False Then
        If MsgBox("请确认该表第一行为标题行，且名称不重复，其他行为具体数据", vbOKCancel) = vbOK Then
            On Error GoTo cannotReName
            ActiveSheet.Name = "浅北来源表"
        End If
    Else
        MsgBox "已存在同名工作表，请检查后重试"
    End If
    Exit Sub

cannotReName:
    MsgBox "不能重命名工作表，可能是因为开启了工作簿保护"
End Sub
Sub setTemplate(control As IRibbonControl)
    If hasWorkSheet("浅北模板表") = False Then
        On Error GoTo cannotReName
        ActiveSheet.Name = "浅北模板表"
    Else
        MsgBox "已存在同名工作表，请检查后重试"
    End If
    Exit Sub
cannotReName:
    MsgBox "不能重命名工作表，可能是因为开启了工作簿保护"
End Sub
Sub routineFormat(control As IRibbonControl)
    Dim rng As Range
    For Each rng In Selection
        If rng <> "" Then rng.Value = ">" & rng.Value & "<"
    Next
End Sub
Sub textFormat(control As IRibbonControl)
    Dim rng As Range
    For Each rng In Selection
        If rng <> "" Then rng.Value = "》" & rng.Value & "《"
    Next
End Sub
Sub imgFormat(control As IRibbonControl)
    Dim rng As Range
    For Each rng In Selection
        If rng <> "" Then rng.Value = "》" & rng.Value & "<"
    Next
End Sub
'开始合并
Sub startMerge(control As IRibbonControl)

    '判断是否存在模板表及来源表
    If hasWorkSheet("浅北来源表") And hasWorkSheet("浅北模板表") Then

        On Error GoTo cancelSelect
        Worksheets("浅北模板表").Select

        On Error GoTo 0

        '获取选区中符合规范的单元格的相对地址字符串
        Dim routineCell(), imgCell(), textCell() '一维数组，存放单元格在模板表的相对地址字符串
        Dim routineCol(), imgCol(), textCol() '一维数组，存放该单元格在来源表的列号

        '在选择的区域中遍历，找到对应地址字符串及来源列号
        Dim c As Range, i As Long, j As Long, k As Long, Co As Long
        For Each c In Worksheets("浅北模板表").UsedRange
            With Worksheets("浅北来源表")
                If Left(c.Value, 1) = ">" And Right(c.Value, 1) = "<" Then
                    ReDim Preserve routineCell(i), routineCol(i)
                    routineCell(i) = c.address(0, 0)
                    For Co = 1 To .UsedRange.Columns.Count
                        If .Cells(1, Co).Value = Mid(c.Value, 2, Len(c.Value) - 2) Then
                            routineCol(i) = Co
                            Exit For
                        End If
                    Next
                    i = i + 1
                ElseIf Left(c.Value, 1) = "》" And Right(c.Value, 1) = "《" Then
                    ReDim Preserve textCell(j), textCol(j)
                    textCell(j) = c.address(0, 0)
                    For Co = 1 To .UsedRange.Columns.Count
                        If .Cells(1, Co).Value = Mid(c.Value, 2, Len(c.Value) - 2) Then
                            textCol(j) = Co
                            Exit For
                        End If
                    Next
                    j = j + 1
                ElseIf Left(c.Value, 1) = "》" And Right(c.Value, 1) = "<" Then
                    ReDim Preserve imgCell(k), imgCol(k)
                    imgCell(k) = c.address(0, 0)
                    For Co = 1 To .UsedRange.Columns.Count
                        If .Cells(1, Co).Value = Mid(c.Value, 2, Len(c.Value) - 2) Then
                            imgCol(k) = Co
                            Exit For
                        End If
                    Next
                    k = k + 1
                Else
                End If
            End With
        Next

        '重命名sheet的依据
        Worksheets("浅北来源表").Select
        On Error GoTo cancelSelect
        Dim byCol As Range
        Set byCol = Application.InputBox("请定位到命名依据列单元格？（该列不能有重复值）", Type:=8)

        Application.ScreenUpdating = False

        '复制表并修改里面的内容
                    
        Dim ro As Long: ro = 2
        With Worksheets("浅北来源表")
            Do While .Range("A" & ro) <> 0

                    Dim curSht As Worksheet
                    On Error Resume Next        '防止数组中没有数据时出错
                    Worksheets("浅北模板表").Copy before:=Worksheets(1)
                    Set curSht = Worksheets(1)

                    curSht.Name = .Cells(ro, byCol.Column).Value

                    For i = LBound(routineCell) To UBound(routineCell)
                        If routineCol(i) Then
                            curSht.Range(routineCell(i)).Value = .Cells(ro, routineCol(i)).Value
                        End If
                    Next
                    For j = LBound(textCell) To UBound(textCell)
                        If textCol(j) Then
                            curSht.Range(textCell(j)).Value = .Cells(ro, textCol(j)).Value
                        End If
                    Next
                    For k = LBound(imgCell) To UBound(imgCell)
                        If imgCol(k) Then
                            curSht.Range(imgCell(k)).Value = .Cells(ro, imgCol(k)).Value
                            Dim shp As Shape
                            Set shp = ActiveSheet.Shapes.AddPicture(curSht.Range(imgCell(k)).Value, msoFalse, msoCTrue, curSht.Range(imgCell(k)).MergeArea.Left, curSht.Range(imgCell(k)).MergeArea.Top, curSht.Range(imgCell(k)).MergeArea.width, curSht.Range(imgCell(k)).MergeArea.Height) '可匹配合并单元格
                            shp.Placement = xlMoveAndSize '随单元格大小和位置改变
                        End If
                    Next
                    ro = ro + 1
            Loop
        End With

        Application.ScreenUpdating = True
        Worksheets("浅北来源表").Select

    Else
            MsgBox "请先设置来源表和模板表"
    End If
    Exit Sub
    
cancelSelect:
    MsgBox "已取消选择，操作已中止"
End Sub

'汇总表格
Sub worksheetsInOne(control As IRibbonControl)
    
    Dim newsht As Worksheet
    Dim arr()

    Dim i, j, k, con As Integer
    
    If MsgBox("请确保已选中需要合并的表格", vbOKCancel) = vbOK Then
        Dim rng As Range
        Set rng = Application.InputBox(" 复制各表格的哪个区域？", Type:=8)
        If Err Then Exit Sub

        Dim rngaddress
        Dim colcount, rowcount, hangshu As Integer
        If VBA.Strings.InStr("!", rng.address) <> 0 Then
            rngaddress = Split(rng.address, "!")(1)
        Else
            rngaddress = rng.address
        End If
    
        colcount = rng.Columns.Count
        rowcount = rng.Rows.Count
        hangshu = InputBox("请输入每行放多少个表格")

        Excel.Application.ScreenUpdating = False
    
        con = ActiveWindow.SelectedSheets.Count
        ReDim arr(1 To con)
    
        i = 1
        Dim sht As Worksheet
        For Each sht In ActiveWindow.SelectedSheets
            arr(i) = sht.Name
            i = i + 1
        Next
    
        Set newsht = Sheets.Add(before:=Sheets(1), Count:=1)
        Dim shtName
        shtName = newsht.Name
    
        i = 1
        j = 1
        For k = LBound(arr) To UBound(arr)
            Sheets(arr(k)).Range(rngaddress).Copy Sheets(shtName).Cells(i, j)
    
            If j <= (hangshu - 1) * colcount Then
                j = j + colcount
            Else
                i = i + rowcount
                j = 1
            End If
        Next
    
    End If
    
    Excel.Application.ScreenUpdating = True

End Sub

Sub csvToXlsx(control As IRibbonControl)
    'csv文件不能超过1048576行，否则会出错
    If MsgBox("请确认csv文件不超过工作表行数", vbOKCancel) <> vbOK Then Exit Sub

    Application.ScreenUpdating = False
    
    Dim str()
    On Error Resume Next
    str = Application.GetOpenFilename("csv文件(*.csv),*.csv", Title:="请选择要转换的文件", MultiSelect:=True)
    
    Dim i As Integer
    For i = LBound(str) To UBound(str)
        Dim wb As Workbook
        Set wb = Workbooks.Open(str(i), ReadOnly:=True)
        '保存为默认工作簿+常规工作簿文件
        wb.SaveAs Replace(str(i), ".csv", ""), IIf(Application.VERSION >= 12, xlWorkbookDefault, xlWorkbookNormal)
        wb.Close
    Next
    
    Application.ScreenUpdating = True
End Sub
Sub xlamToXls(control As IRibbonControl)
    Dim strFile, wb As Workbook
    strFile = Application.GetOpenFilename(FileFilter:="Micrsoft Excel文件(*.xlam), *.xlam")
    If strFile = False Then Exit Sub
    With Workbooks.Open(strFile)
        .IsAddin = False
        .SaveAs FileName:=Replace(strFile, "xlam", "xls"), FileFormat:=xlExcel8
        .Close
    End With
End Sub

' 旧Excel文件破解
' 主要针对 VBA 代码
Sub fileDecryption(control As IRibbonControl)
    On Error Resume Next
    Dim FileName
    FileName = Application.GetOpenFilename("Excel文件,*.xls;*.xla;*.xlt", , "旧格式文件破解")
    If Dir(FileName) = "" Then
        MsgBox "没找到相关文件,请重新设置"
        Exit Sub
    Else
        FileCopy FileName, FileName & ".bak" '备份文件
    End If

    Dim GetData As String * 5
    Open FileName For Binary As #1
    Dim CMGs As Long
    Dim DPBo As Long
    Dim i
    For i = 1 To LOF(1)
        Get #1, i, GetData
        If GetData = "CMG=""" Then CMGs = i
        If GetData = "[Host" Then DPBo = i - 2: Exit For
    Next
    If CMGs = 0 Then
        MsgBox "请先对VBA编码设置一个保护密码...", 32, "提示"
        Exit Sub
    End If
    
    Dim St As String * 2
    Dim s20 As String * 1
    '取得一个0D0A十六进制字串
    Get #1, CMGs - 2, St
    '取得一个20十六制字串
    Get #1, DPBo + 16, s20
    '替换加密部份机码
    For i = CMGs To DPBo Step 2
        Put #1, i, St
    Next
    '加入不配对符号
    If (DPBo - CMGs) Mod 2 <> 0 Then
        Put #1, DPBo + 1, s20
    End If
    MsgBox "文件解密成功！", 32, "提示"
    Close #1
End Sub
'========================
'
'    视 图 与 安 全
'
'========================


'设置可编辑区域()
Sub setEditableRange(control As IRibbonControl)

    On Error GoTo cannotProtect
    Selection.Locked = False
    Selection.FormulaHidden = False
    
    Dim str As String
    str = InputBox("请输入加密密码（可为空）")
    MsgBox "请记住你的工作表加密密码：“" & str & "”"
    
    ' 允许使用自动筛选功能
    ActiveSheet.Protect Password:=str, DrawingObjects:=True, Contents:=True, Scenarios:=True, AllowFiltering:=True
    ActiveSheet.EnableSelection = xlUnlockedCells
    Exit Sub
    
cannotProtect:
    MsgBox "请先设置单元格锁定/撤销工作表保护！"
End Sub

'撤销工作表保护()
Sub unrestrictedEditableRange(control As IRibbonControl)
    On Error Resume Next
    ActiveSheet.Unprotect
    If Err Then
        MsgBox "您输入的密码不正确！"
        Exit Sub
    Else
        ActiveSheet.Cells.Locked = True '所有单元格锁定状态恢复默认
        MsgBox "已撤销工作表密码保护！"
    End If
End Sub


'保护工作簿结构()
Sub openWorkbookProtection(control As IRibbonControl)
    On Error GoTo cannotProtect
    ActiveWorkbook.Unprotect

    Dim str As String
    str = InputBox("请输入工作簿保护密码（可为空）")    '点击取消会视为空字符串

    ActiveWorkbook.Protect Password:=str, Structure:=True, Windows:=False
    MsgBox "请记住你的工作簿结构保护密码：“" & str & "”"
    Exit Sub

cannotProtect:
    MsgBox "请解除工作簿保护后重试"
End Sub

' 关闭工作簿结构保护，需要密码
Sub closeWorkbookProtection(control As IRibbonControl)
    
    Dim str As String
    str = InputBox("请输入解锁工作簿结构保护密码")
    If Err Then Exit Sub
    On Error GoTo cannotUnprotect
    ActiveWorkbook.Unprotect str
    MsgBox "已解除工作簿结构保护！"
    Exit Sub

cannotUnprotect:
    MsgBox "您输入的密码不正确，请稍后再试"
End Sub

' 破解工作表保护
Sub crackWorkSheetsProtection(control As IRibbonControl)
    ' 用于单元格无法修改

    If MsgBox("如果单元格无法修改可使用此功能！" & Chr(13) & Chr(13) & "此功能仅限于自己的文件忘记密码时使用，勿做他用", vbOKCancel + vbCritical) = vbOK Then
        Application.ScreenUpdating = False
        Application.DisplayAlerts = False
        On Error Resume Next
        Dim sht As Worksheet
        For Each sht In Worksheets
            ' 需要设置密码为""，否则会弹窗提示录入密码
            sht.Protect Password:="", DrawingObjects:=True, Contents:=True, Scenarios:=True, AllowFiltering:=True, AllowUsingPivotTables:=True
            sht.Protect Password:="", DrawingObjects:=False, Contents:=True, Scenarios:=False, AllowFiltering:=True, AllowUsingPivotTables:=True
            sht.Protect Password:="", DrawingObjects:=True, Contents:=True, Scenarios:=False, AllowFiltering:=True, AllowUsingPivotTables:=True
            sht.Protect Password:="", DrawingObjects:=False, Contents:=True, Scenarios:=True, AllowFiltering:=True, AllowUsingPivotTables:=True
            sht.Unprotect Password:=""
        Next
        Application.DisplayAlerts = True
        Application.ScreenUpdating = True
        MsgBox "密码已被取消"
    End If
End Sub

' 破解工作簿保护
Sub crackWorkbookProtection(control As IRibbonControl)
    If MsgBox("此功能会创建一个可编辑的副本文件，请注意及时保存！" & Chr(13) & Chr(13) & "此功能仅限于自己的文件忘记密码时使用，勿做他用。", vbCritical + vbOKCancel) = vbOK Then
        Dim activeSheetName As String   ' 保存当前选中工作表的状态
        activeSheetName = ActiveSheet.Name
        ActiveWorkbook.Sheets.Copy
        Dim sht As Worksheet
        For Each sht In ActiveWorkbook.Sheets
            sht.Visible = True
            If sht.Name = activeSheetName Then
                sht.Select
            End If
        Next
    End If
End Sub


Private Sub SplitSpreadMergeCell(control As IRibbonControl)
    If MsgBox("此功能会拆分所有列中，纵向跨页的合并单元格，操作无法撤销，请确认！", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim view
    view = ActiveWindow.view
    
    Application.ScreenUpdating = False
    
    ' 获取使用区域的开始结束范围
    Dim startCol As Long, EndCol As Long
    startCol = ActiveSheet.UsedRange.Cells(1).Column
    EndCol = ActiveSheet.UsedRange.Cells(ActiveSheet.UsedRange.Cells.Count).Column
    
    ' 获取分页线所在的合并单元格
    ActiveWindow.view = xlPageBreakPreview  '切换到打印视图
    
    Dim hpb As HPageBreak
    For Each hpb In ActiveSheet.HPageBreaks
        Dim i As Long
        For i = startCol To EndCol
            'hpb.Location 分页线下方第一个单元格

            Dim PageCell As Range
            Set PageCell = Cells(hpb.Location.Row - 1, i)
            
            ' 如果此单元格是合并单元格并且与分页线相交
            If PageCell.mergeCells And Not Intersect(Cells(hpb.Location.Row, i), PageCell.MergeArea) Is Nothing Then
                ' 获取此时合并单元格的信息
                Dim MergeAddress As String, MergeValue
                MergeAddress = PageCell.MergeArea.address   ' 必须先获取地址，否则拆分后就变成了一个单元格
                MergeValue = PageCell.MergeArea(1).Value
                
                ' 普通的粘贴格式会把合并属性也粘贴
                ' 只能将要格式先存储起来，之后再复制
                
                ' 以下内容经过测试，可以自动填充应用
                ' 数字格式
                ' 背景填充
                ' 字体家族、大小、颜色
                ' 对齐
                ' 自动换行
                ' 只需要重新设置：边框
                Dim topBorder As border, bottomBorder As border, leftBorder As border, rightBorder As border
                Dim iHBorder As border, iVBorder As border
                Set topBorder = PageCell.MergeArea.Borders(xlEdgeTop)
                Set bottomBorder = PageCell.MergeArea.Borders(xlEdgeBottom)
                Set leftBorder = PageCell.MergeArea.Borders(xlEdgeLeft)
                Set rightBorder = PageCell.MergeArea.Borders(xlEdgeRight)
                Set iVBorder = PageCell.MergeArea.Borders(xlInsideVertical)
                Set iHBorder = PageCell.MergeArea.Borders(xlInsideHorizontal)

                ' 拆分单元格
                PageCell.MergeArea.mergeCells = False
                
                ' 依序合并成两个合并单元格
                Dim firstAddress As String, endAddress As String
                firstAddress = Split(MergeAddress, ":")(0)
                endAddress = Split(MergeAddress, ":")(1)
                
                On Error Resume Next
                With Range(Range(firstAddress), Cells(hpb.Location.Row - 1, i))
                    .Merge
                    .Value = MergeValue
                    With .Borders(xlEdgeTop)
                        .LineStyle = topBorder.LineStyle
                        .ColorIndex = topBorder.ColorIndex
                        .Color = topBorder.Color
                        .Weight = topBorder.Weight
                    End With
                    With .Borders(xlEdgeBottom)
                        .LineStyle = bottomBorder.LineStyle
                        .ColorIndex = bottomBorder.ColorIndex
                        .Color = bottomBorder.Color
                        .Weight = bottomBorder.Weight
                    End With
                    With .Borders(xlEdgeLeft)
                        .LineStyle = leftBorder.LineStyle
                        .ColorIndex = leftBorder.ColorIndex
                        .Color = leftBorder.Color
                        .Weight = leftBorder.Weight
                    End With
                    With .Borders(xlEdgeRight)
                        .LineStyle = rightBorder.LineStyle
                        .ColorIndex = rightBorder.ColorIndex
                        .Color = rightBorder.Color
                        .Weight = rightBorder.Weight
                    End With
                    With .Borders(xlInsideHorizontal)
                        .LineStyle = iHBorder.LineStyle
                        .ColorIndex = iHBorder.ColorIndex
                        .Color = rightBorder.Color
                        .Weight = iHBorder.Weight
                    End With
                End With
                
                With Range(Cells(hpb.Location.Row, i), Range(endAddress))
                    .Merge
                    .Value = MergeValue
                    With .Borders(xlEdgeTop)
                        .LineStyle = topBorder.LineStyle
                        .ColorIndex = topBorder.ColorIndex
                        .Color = topBorder.Color
                        .Weight = topBorder.Weight
                    End With
                    With .Borders(xlEdgeBottom)
                        .LineStyle = bottomBorder.LineStyle
                        .ColorIndex = bottomBorder.ColorIndex
                        .Color = bottomBorder.Color
                        .Weight = bottomBorder.Weight
                    End With
                    With .Borders(xlEdgeLeft)
                        .LineStyle = leftBorder.LineStyle
                        .ColorIndex = leftBorder.ColorIndex
                        .Color = leftBorder.Color
                        .Weight = leftBorder.Weight
                    End With
                    With .Borders(xlEdgeRight)
                        .LineStyle = rightBorder.LineStyle
                        .ColorIndex = rightBorder.ColorIndex
                        .Color = rightBorder.Color
                        .Weight = rightBorder.Weight
                    End With
                    With .Borders(xlInsideHorizontal)
                        .LineStyle = iHBorder.LineStyle
                        .ColorIndex = iHBorder.ColorIndex
                        .Color = rightBorder.Color
                        .Weight = iHBorder.Weight
                    End With
                End With
                
                'With Range(Range(firstAddress), Range(endAddress))
                    
                'End With
            End If
        Next
    Next
    Application.ScreenUpdating = True
    ActiveWindow.view = view
    MsgBox "此操作虽然复制了边框线，但在打印时可能还是无框线，请检查后再打印"
    
End Sub

' 分页线

Sub getPressedDisplayPageBreaks(control As IRibbonControl, ByRef returnedVal)
    returnedVal = ActiveSheet.DisplayPageBreaks
End Sub

Sub switchDisplayPageBreaks(control As IRibbonControl, pressed As Boolean)
    ActiveSheet.DisplayPageBreaks = pressed
End Sub


'========================
'
'       关  于
'
'========================
Sub aboutSoft(control As IRibbonControl)
    aboutForm.Show
End Sub

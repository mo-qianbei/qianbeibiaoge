Attribute VB_Name = "Fun"
Option Explicit '强制变量声明
'Option Base 0 '数组下标从0开始

'函数功能介绍，无法放在程序启动时加载，需要点击按钮调用
'Excel 2007 版本会报错，无法使用
Sub loadFun()

    On Error GoTo ErrVersion        '如果添加出错，跳转文字版函数介绍网页

    Dim JVLOOKUPArg(1 To 3) As String
    JVLOOKUPArg(1) = "【查找值】需要在数据表进行搜索的值，可以是数值、引用或字符串"
    JVLOOKUPArg(2) = "【搜索区域】在哪里找"
    JVLOOKUPArg(3) = "【相对位置】如果找到了该数据，要返回它右侧的第几个单元格的数据。0为数据本身，负数表示向左查找"
    
    Excel.Application.MacroOptions "JVLOOKUP", _
    "搜索表区域中满足条件的第一个单元格。如果找到，返回它右侧第±N个单元格的值；未找到返回0", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:=JVLOOKUPArg

'-------------------------------------------------------------------------------------------------

    Dim JRANKArg(1 To 3) As String
    JRANKArg(1) = "【数字】找哪个数字的排名"
    JRANKArg(2) = "【区域】这个数在哪个区域的排名"
    JRANKArg(3) = "[排序方式] 指定排名的方式。0或忽略，降序（值越大排名越靠前）；1:反之"
    Excel.Application.MacroOptions "JRANK", _
    "【中国式排名】返回某数字在一列数字中相对于其他数值的大小排名。排名并列相同，不占用名次。" & Chr(10) & "eg：有并列第2，则第4名自动变为第3名（122345）", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:=JRANKArg

'-------------------------------------------------------------------------------------------------

    Dim JSHENFENZHENGArg(1 To 2) As String
    JSHENFENZHENGArg(1) = "【身份证号】15位或18位字符"
    JSHENFENZHENGArg(2) = "[2信息类型] 获取身份证中的哪种信息" & Chr(10) & _
      "1:地区  2:生日  3:年龄  4:生肖  5:星座  6:性别" & Chr(10) & _
      "7:是否合规（默认）  8:校验码  9:转18位号码"
    Excel.Application.MacroOptions "JSHENFENZHENG", _
    "【身份证信息】获取中国大陆居民身份证包含的信息", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:=JSHENFENZHENGArg

'-------------------------------------------------------------------------------------------------

    Excel.Application.MacroOptions "JHYPELINK", _
    "【提取链接】返回单元格设置的链接，如果未找到，返回错误值", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:="需要获取链接的单元格"

'-------------------------------------------------------------------------------------------------

    Excel.Application.MacroOptions "JRANDNAME", _
    "【随机姓名】返回一个随机的中文姓名", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:="[性别] 1:男  0:女  2:随机（默认值）"
    
'-------------------------------------------------------------------------------------------------
    Dim JSPLITArg(1 To 3) As String
    JSPLITArg(1) = "【文本】需要截取的字符"
    JSPLITArg(2) = "【第1个分隔符】"
    JSPLITArg(3) = "【第2个分隔符】"
    Excel.Application.MacroOptions "JSPLIT", _
    "【截取字符】截取文本中位于两个分隔符之间的文本", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:=JSPLITArg

    Dim JREPLACEArg(1 To 3) As String
    JREPLACEArg(1) = "【文本】需要替换的旧字符串"
    JREPLACEArg(2) = "【查找字符】"
    JREPLACEArg(3) = "【替换字符】"
    Excel.Application.MacroOptions "JREPLACE", _
    "【替换文本】返回将旧文本中的字符替换为新字符形成的新文本", _
    Category:="浅北表格助手", _
    ArgumentDescriptions:=JREPLACEArg
    
    Dim JRegRepArg(1 To 4) As String
    JRegRepArg(1) = "【要在哪个字符串中找】"
    JRegRepArg(2) = "【查找规则】正则表达式"
    JRegRepArg(3) = "[替换内容]默认为空"
    JRegRepArg(4) = "[全局匹配]布尔值，默认为True"
    Excel.Application.MacroOptions "JReg_Replace", _
      "正则表达式替换", _
      Category:="浅北表格助手", _
      ArgumentDescriptions:=JRegRepArg

    Dim JRegCouArg(1 To 3) As String
    JRegCouArg(1) = "【要在哪个字符串中找】"
    JRegCouArg(2) = "【查找规则】正则表达式"
    JRegCouArg(3) = "[全局匹配]布尔值，默认为True"
    
    Excel.Application.MacroOptions "JReg_Count", _
      "正则表达式匹配个数", _
      Category:="浅北表格助手", _
      ArgumentDescriptions:=JRegCouArg

    Dim JRegExeArg(1 To 4) As String
    JRegExeArg(1) = "【要在哪个字符串中找】"
    JRegExeArg(2) = "【查找规则】正则表达式"
    JRegExeArg(3) = "[查找第几个]默认为0（第一个）"
    JRegExeArg(4) = "[全局匹配]布尔值，默认为True"
    Excel.Application.MacroOptions "JReg_Execute", _
      "正则表达式匹配", _
      Category:="浅北表格助手", _
      ArgumentDescriptions:=JRegExeArg

    Dim JCnArg(1 To 3) As String
    JCnArg(1) = "【要转换的中文数字字符串】"
    JCnArg(2) = "[从 text 的第几个字符开始查找]，默认为1"
    JCnArg(3) = "[自 start_num 后查找多少位字符]，默认最多255"
    Excel.Application.MacroOptions "JCnToAn", _
      "中文数字转阿拉伯数字", _
      Category:="浅北表格助手", _
      ArgumentDescriptions:=JCnArg

    MsgBox "现在可以到函数详情中查看具体参数的使用说明了"
    Exit Sub

ErrVersion:
    If MsgBox("您当前版本软件不支持添加自定义函数提示，是否现在查看文字版函数说明？", vbOKCancel) = vbOK Then
        ActiveWorkbook.FollowHyperlink address:="https://www.yuque.com/moqianbei/qianbeibiaoge/function", NewWindow:=True
    End If
End Sub



Function JVLOOKUP(Lookup_value As String, Table_array As Range, Offset_num As Integer)
Attribute JVLOOKUP.VB_Description = "搜索表区域中满足条件的第一个单元格。如果找到，返回它右侧第±N个单元格的值；未找到返回0"
Attribute JVLOOKUP.VB_ProcData.VB_Invoke_Func = " \n20"

    '使用vba的find函数，实现工作表的match、index功能
    '未找到会返回数字0
    
    Dim rng As Range
    Set rng = Table_array.Find(Lookup_value, , , xlWhole)
    If Not rng Is Nothing Then
        JVLOOKUP = rng.Offset(0, Offset_num).Value '右侧为正，左侧为负
    End If

End Function
'中国式排名12234445
Function JRANK(Number As Double, Ref As Range, Optional Order = 0)
Attribute JRANK.VB_Description = "【中国式排名】返回某数字在一列数字中相对于其他数值的大小排名。排名并列相同，不占用名次。\neg：有并列第2，则第4名自动变为第3名（122345）"
Attribute JRANK.VB_ProcData.VB_Invoke_Func = " \n20"
    On Error Resume Next
    Dim rng, i As Integer, Only As New Collection
    For Each rng In Ref
        '如果rng 大于成绩，则将其导入到 only 集合中，用于去除重复值
        If Order = False Then   '为 0 或忽略，降序，数字越大，排名越好
            If rng > Number Then Only.Add rng, CStr(rng)
        Else            '升序，数字越大，排名越靠后
            If rng < Number Then Only.Add rng, CStr(rng)
        End If
    Next
    '将集合only的数据个数加1后赋值给函数，作为做终结果
    JRANK = Only.Count + 1
End Function
'条件排名
'英式排名=SUMPRODUCT((条件区域1=条件1)* (条件区域2=条件2)* (数据区域>数据))
Function PM(igji As Double, quyu As Range, Optional criferia1_range As Range, Optional criferia1 As Range, Optional criferia2_range As Range = 1, Optional criferia2 As Range, Optional criferia3_range As Range = 1, Optional criferia3 As Range = 1)
    'Set zhipianyi = quyu.Offset(1, 0).Resize(quyu.Rows.Count + 1)
    PM.FormulaArray = " Sum((Frequency((criferia1_range = criferia1) * (criferia1_range = criferia2) * (criferia3_range = criferia3) * 值所在区域, 值所在区域) > 0) * (qy.Offset(-1, 0).Resize(qy.Rows.Count + 1) > igji)) + 1"
End Function

Function JHYPELINK(Ref)
Attribute JHYPELINK.VB_Description = "【提取链接】返回单元格设置的链接，如果未找到，返回错误值"
Attribute JHYPELINK.VB_ProcData.VB_Invoke_Func = " \n20"
'获取某单元格的内部链接
    Application.Volatile True
    With Ref.Hyperlinks(1)
        JHYPELINK = IIf(.address = "", .SubAddress, .address)
    End With
End Function

Function JSHENFENZHENG(id As String, Optional getType As Integer = 7)
Attribute JSHENFENZHENG.VB_Description = "【身份证信息】获取中国大陆居民身份证包含的信息"
Attribute JSHENFENZHENG.VB_ProcData.VB_Invoke_Func = " \n20"
    Dim shenfenID As New ClassIDCard
    If getType <> 0 Then
        JSHENFENZHENG = shenfenID.info(id, getType)
    Else    ' 隐藏功能，类型为0时，取得校验码
        JSHENFENZHENG = shenfenID.CRC(id)
    End If
End Function

'1为男，0为女，2随机
Function JRANDNAME(Optional ByVal Sex As Byte = 2)
Attribute JRANDNAME.VB_Description = "【随机姓名】返回一个随机的中文姓名"
Attribute JRANDNAME.VB_ProcData.VB_Invoke_Func = " \n20"

    Dim xingming As String, xingshi As String, nanxingming As String, nvxingming As String
    
    '避免重复调用影响运行时间，先进行读取
    xingshi = DATA_xingshi
    nanxingming = DATA_nanxingming
    nvxingming = DATA_nvxingming
    
    If Sex = 1 Then
        xingming = Split(xingshi, ",")(Int(Rnd * CountX(xingshi, ",") + 1)) & _
        Split(nanxingming, ",")(Int(Rnd * CountX(nanxingming, ",") + 1))
    
    ElseIf Sex = 0 Then
        xingming = Split(xingshi, ",")(Int(Rnd * CountX(xingshi, ",") + 1)) & _
        Split(nvxingming, ",")(Int(Rnd * CountX(nvxingming, ",") + 1))
    Else
        xingming = Split(xingshi, ",")(Int(Rnd * CountX(xingshi, ",") + 1)) & _
        Split(nanxingming & nvxingming, ",")(Int(Rnd * CountX(DATA_nanxingming & nvxingming, ",") + 1))
    End If
    JRANDNAME = xingming
End Function

'截取文本中两个分隔符之间的文本
Function JSPLIT(find_text As Range, splitter1 As String, splitter2 As String) As String
Attribute JSPLIT.VB_Description = "【截取字符】截取文本中位于两个分隔符之间的文本"
Attribute JSPLIT.VB_ProcData.VB_Invoke_Func = " \n20"

    Dim temp As String
    '如果可以找到分隔的字符串1，那么取后半段
    If InStr(find_text, splitter1) <> 0 Then
        temp = Split(find_text, splitter1)(1)
        If InStr(temp, splitter2) <> 0 Then
            JSPLIT = Split(temp, splitter2)(0)
        Else
            JSPLIT = "第2分隔符不正确"
        End If
    Else
        JSPLIT = "第1分隔符不正确"
    End If

End Function

'替换文本内容
Function JREPLACE(old_text As Range, find_text As String, replace_text As String) As String
Attribute JREPLACE.VB_Description = "【替换文本】返回将旧文本中的字符替换为新字符形成的新文本"
Attribute JREPLACE.VB_ProcData.VB_Invoke_Func = " \n20"
    JREPLACE = Replace(old_text, find_text, replace_text)
End Function


' 正则替换
Public Function JReg_Replace(ByVal text As String, ByVal patternStr As String, Optional ByVal replaceStr As String = "", Optional ByVal isGlobal As Boolean = True) As String
Attribute JReg_Replace.VB_Description = "正则表达式替换"
Attribute JReg_Replace.VB_ProcData.VB_Invoke_Func = " \n20"
    Dim Reg As Object
    Set Reg = CreateObject("vbscript.regexp")
    With Reg
        .Pattern = patternStr
        .Global = isGlobal
        JReg_Replace = .Replace(text, replaceStr)
    End With
End Function

' 正则查找
' 查找匹配的项，从1开始计数
' 如果录入0，则返回所有匹配项的数组
Public Function JReg_Execute(ByVal text As String, ByVal patternStr As String, Optional ByVal strIndex As Long = 1, Optional ByVal isGlobal As Boolean = True) As String
Attribute JReg_Execute.VB_Description = "正则表达式匹配"
Attribute JReg_Execute.VB_ProcData.VB_Invoke_Func = " \n20"

    Dim Reg As Object
    Set Reg = CreateObject("vbscript.regexp")
    With Reg
        .Global = isGlobal
        .Pattern = patternStr
        Dim cnt As Long
        cnt = .Execute(text).Count
        If cnt > 0 Then
            Debug.Print cnt
            Dim arr, i
            ReDim arr(1 To 1, 1 To cnt)
            For i = 0 To cnt - 1
                arr(1, i + 1) = .Execute(text)(i).Value
            Next
            
            ' vba自定义函数不支持自动溢出，会报错
            
            If strIndex = 0 Then
                JReg_Execute = arr
            ElseIf strIndex > 0 Then
                JReg_Execute = arr(1, strIndex)
            Else
                JReg_Execute = arr(1, cnt + strIndex + 1)
            End If
        Else
            JReg_Execute = ""
        End If
    End With

End Function


' 正则匹配
' 符合条件匹配的数量
Public Function JReg_Count(ByVal text As String, ByVal patternStr As String, Optional ByVal isGlobal As Boolean = True) As Long
Attribute JReg_Count.VB_Description = "正则表达式匹配个数"
Attribute JReg_Count.VB_ProcData.VB_Invoke_Func = " \n20"

    Dim Reg As Object
    Set Reg = CreateObject("vbscript.regexp")

    With Reg
        .Global = isGlobal
        .Pattern = patternStr
        JReg_Count = .Execute(text).Count
    End With

End Function

' ====== 将中文数字转换为阿利伯数字 ==============='
' text 要转换的中文数字字符串                      '
' start_num 从text 的第几位开始查找                '
' chars_num 自 start_num 后查找多少位字符          '
'--------------------------------------------------'

Public Function JCnToAn(ByVal text As String, Optional start_num As Long = 1, Optional chars_num As Long = 255) As LongPtr
Attribute JCnToAn.VB_Description = "中文数字转阿拉伯数字"
Attribute JCnToAn.VB_ProcData.VB_Invoke_Func = " \n20"

    ' ---- 1. 在用户限定的范围中查找
    Dim temp As String
    temp = Mid(text, start_num, chars_num)


    '  --- 2. 将中文大写转换为中文小写，方便后续统一配置
    temp = Replace(temp, "零", "〇")
    temp = Replace(temp, "壹", "一")
    temp = Replace(temp, "贰", "二")
    'temp = Replace(temp, "两", "二")        ' 兼容“两千三”口语表述，但可能与单位“两”混淆“四两三”
    temp = Replace(temp, "叁", "三")
    temp = Replace(temp, "肆", "四")
    temp = Replace(temp, "伍", "五")
    temp = Replace(temp, "陆", "六")
    temp = Replace(temp, "柒", "七")
    temp = Replace(temp, "捌", "八")
    temp = Replace(temp, "玖", "九")
    temp = Replace(temp, "拾", "十")
    temp = Replace(temp, "佰", "百")
    temp = Replace(temp, "仟", "千")
    temp = Replace(temp, "萬", "万")
    temp = Replace(temp, "億", "亿")


    '  --- 3. 精确获取纯数字中文字符
    Dim Reg As Object
    Set Reg = CreateObject("VBScript.RegExp")
    With Reg
        .Pattern = "[〇一二三四五六七八九十百千万亿]+"
        temp = .Execute(temp)(0)    ' 查找到第一个匹配的纯中文汉字
    End With


    ' --- 4. 处理特殊口语表达形式：一万二、二千三、三百四

    If InStr(temp, "百") > 0 Then
        If Len(Split(temp, "百")(1)) = 1 Then
            temp = temp & "十"
        End If
    End If
    If InStr(temp, "千") > 0 Then
        If Len(Split(temp, "千")(1)) = 1 Then
            temp = temp & "百"
        End If
    End If
    If InStr(temp, "万") > 0 Then
        If Len(Split(temp, "万")(1)) = 1 Then
            temp = temp & "千"
        End If
    End If


    ' --- 5. 获取高、中、低位的中文汉字及阿拉伯数字
    ' 高位（超过一亿）、中位（超过一万但小于一亿）、低位（小于一万）
    Dim uNum As Integer, mNum As Integer, lNum As Integer
    Dim uStr As String, mStr As String, lStr As String, tStr As String

    ' 获取高位数字（超过一亿）
    If InStr(temp, "亿") > 0 Then
        uStr = Split(temp, "亿")(0)
        uNum = fourDig(uStr)
        tStr = Split(temp, "亿")(1)
    Else
        uNum = 0
        tStr = temp
    End If
    
    ' 获取中位（超过一万但小于一亿）、低位数字（小于一万）
    If InStr(tStr, "万") > 0 Then
        mStr = Split(tStr, "万")(0)
        mNum = fourDig(mStr)
        lStr = Split(tStr, "万")(1)
        lNum = fourDig(lStr)
    Else
        mNum = 0
        lStr = tStr
        lNum = fourDig(lStr)
    End If

    JCnToAn = uNum * 100000000# + mNum * 10000# + lNum

End Function

Private Function fourDig(ByVal num_str As String) As Integer
    Dim i As Integer
    Dim num As Long
    Dim temp As Long
    Dim str As String
    For i = 1 To Len(num_str)
        str = Mid(num_str, i, 1)
        Select Case str
            Case "零", "〇"
                temp = 0
            Case "一"
                temp = 1
            Case "二", "两"
                temp = 2
            Case "三"
                temp = 3
            Case "四"
                temp = 4
            Case "五"
                temp = 5
            Case "六"
                temp = 6
            Case "七"
                temp = 7
            Case "八"
                temp = 8
            Case "九"
                temp = 9
            Case "十"
                If i = 1 Then   ' 当“十”在开头，当做“一十”处理
                    num = num + 10
                    temp = 0
                Else
                    num = num + temp * 10
                    temp = 0
                End If
            Case "百"
                num = num + temp * 100
                temp = 0
            Case "千"
                num = num + temp * 1000
                temp = 0
        End Select
    Next

    fourDig = num + temp
End Function

'因为采用的是FIND方法，因此不支持*?通配符
'criteriaPairs应该按顺序成对出现：(criteriaRange1, criteria1), (criteriaRange2, criteria2), ...
' lookupRange 和 returnRange 都是单列，且一一对应，总数相同
'Function JVLOOKUPIFS(ByVal lookupValue As Variant, ByVal lookupRange As Range, ByVal returnRange As Range, ByVal forwardSearch As Boolean, ParamArray criteriaPairs() As Variant) As Variant
'
'        If forwardSearch Then
'            Set tempRange = criteriaRange.Find(What:=criteria, LookAt:=xlWhole, SearchDirection:=xlNext)
'        Else
'            Set tempRange = criteriaRange.Find(What:=criteria, LookAt:=xlWhole, SearchDirection:=xlPrevious)
'        End If
'
'        If tempRange Is Nothing Then
'             没有符合条件的
'            JVLOOKUPIFS = CVErr(xlErrNA)
'            Exit Function
'        End If
'
'        Dim firstAddress As String ' 记录第一次找到的位置
'        firstAddress = tempRange.Address
'        MsgBox firstAddress
'        Do
'             如果rngCollection尚未初始化，则将其设置为第一个找到的单元格
'            If tempRangeCollection Is Nothing Then
'                Set tempRangeCollection = tempRange.EntireRow
'            Else
'                 否则将新找到的单元格添加到已存在的rngCollection之后
'                Set tempRangeCollection = Application.Union(tempRangeCollection, tempRange.EntireRow)
'            End If
'
'             寻找下一个匹配项
'            Set tempRange = criteriaRange.FindNext(after:=tempRange)
'
'             防止循环查找
'            If tempRange.Address = firstAddress Then Exit Do
'        Loop While Not tempRange Is Nothing
'
'        Dim rng As Range
'End Function


' 遍历方法，效率可能稍底一些
' 查找区域、条件区域的单元格必须一一对应，不能错位
' criteriaPairs应该按顺序成对出现：(criteriaRange1, criteria1), (criteriaRange2, criteria2), ...
' lookupRange 和 returnRange 都是单列，且一一对应，总数相同
Function JVLOOKUPIFS(ByVal lookupValue As Variant, ByVal lookupRange As Range, ByVal returnRange As Range, ByVal forwardSearch As Boolean, ParamArray criteriaPairs() As Variant) As Variant
    
    If (UBound(criteriaPairs) - LBound(criteriaPairs)) Mod 2 = 0 Then
        'MsgBox "条件区域与条件必须成对出现！"
        JVLOOKUPIFS = CVErr(xlErrRef)
        Exit Function
    End If
    
    If lookupRange.Cells.Count = 0 Then
        'MsgBox "未设置条件区域！"
        JVLOOKUPIFS = CVErr(xlErrRef)
        Exit Function
    End If
    
    Dim rowsCollection() As Long       ' 记录行号的数组

    Dim i As Long, j As Long, r As Long
    ' 获取查找区域中符合查找值的单元格的行号
    r = 1
    For i = 1 To lookupRange.Cells.Count
        If lookupRange.Cells(i).Value = lookupValue Then
            ReDim Preserve rowsCollection(1 To r)
            rowsCollection(r) = lookupRange.Cells(i).Row
            r = r + 1
        End If
    Next
    
    If UBound(rowsCollection) - LBound(rowsCollection) = 0 Then
        JVLOOKUPIFS = CVErr(xlErrNA)
        MsgBox "在查找区域没找到符合条件的查找值"
        Exit Function
    Else
        Dim tempRowsCollection() As Long
        For i = LBound(criteriaPairs) To UBound(criteriaPairs) Step 2
            Dim criteriaRange As Range, criteria As Variant
            Set criteriaRange = criteriaPairs(i)
            criteria = criteriaPairs(i + 1)

            j = 1
            r = 1
            For r = LBound(rowsCollection) To UBound(rowsCollection)
                If criteriaRange.Cells(rowsCollection(r), 1).Value = criteria Then
                    ReDim Preserve tempRowsCollection(1 To j)
                    tempRowsCollection(j) = rowsCollection(r)
                    j = j + 1
                Else
                End If
            Next


            If j = 1 Then  ' 没找到
                JVLOOKUPIFS = CVErr(xlErrNA)
                MsgBox "第" & i & "个条件区域中没找到条件值"
                Exit Function
            Else
                ' 将临时集合中的数据复制到行集合数组中，做下次精简
                j = 1
                r = 1
                For r = LBound(tempRowsCollection) To UBound(tempRowsCollection)
                    ReDim Preserve tempRowsCollection(1 To j)
                    rowsCollection(j) = tempRowsCollection(r)
                    j = j + 1
                Next
            End If
        Next
        
        
        If forwardSearch = True Then
            JVLOOKUPIFS = returnRange(rowsCollection(1), 1)
        Else
            JVLOOKUPIFS = returnRange(rowsCollection(UBound(rowsCollection)), 1)
        End If
    End If

End Function


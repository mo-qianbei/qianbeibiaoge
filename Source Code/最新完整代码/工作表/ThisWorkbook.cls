VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True

Private Sub Workbook_Open()
    If UCase(GetSetting("浅北助手", "浅北表格助手", "风险确认")) <> "1" Then
        If MsgBox("本工具为VBA编写，所有操作不可撤销，如果您继续使用，代表您已接受此风险！" & Chr(10) & Chr(10) & _
          "为减少用户打扰，本提醒仅在第一次安装时显示，后续不再重复提醒", vbCritical + vbYesNo + vbDefaultButton2, "风险确认") = vbYes Then
            Call SaveSetting("浅北助手", "浅北表格助手", "风险确认", "1")
        Else
            MsgBox "因您取消了风险确认，程序稍后将自动退出，如需重新启用，可以关闭Excel后，重新打开此文件重试！"
            ThisWorkbook.Close
        End If
    End If
End Sub

Private Sub 重置为未确认风险状态()
    Call SaveSetting("浅北助手", "浅北表格助手", "风险确认", "0")
    Debug.Print "已初始化"
End Sub

VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} IDCardForm 
   Caption         =   "身份证号信息查询"
   ClientHeight    =   4740
   ClientLeft      =   110
   ClientTop       =   460
   ClientWidth     =   4440
   OleObjectBlob   =   "IDCardForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  '所有者中心
End
Attribute VB_Name = "IDCardForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private shtEvent As classSheetEvent        ' 定义变量，在整个窗体加载过程中都存在

Private Sub UserForm_Initialize()
    ' 因为在加载前，还没有添加事件，因此需要先获取一次
    Me.RangeTextBox.Value = Selection.address(0, 0)

    Set shtEvent = New classSheetEvent
    Set shtEvent.Worksheet = ActiveSheet

End Sub
Private Sub UserForm_Deactivate()
    Set shtEvent = Nothing  ' 释放对象
End Sub

Private Sub ExitCommandButton_Click()
    Unload Me
End Sub

Private Sub RangeTextBox_Change()
    On Error Resume Next
        Range(Me.RangeTextBox.Value).Select ' 选中录入的单元格
    On Error GoTo 0
    Call init
    
    If InStr(1, Selection.address(0, 0), ":") <= 0 Then ' 如果是单个单元格
        ' 如果单元格存储的是错误值，比如#VALUE等，会报错
        If IsError(Selection.Value) Then
            Exit Sub
        End If
        Dim id As String
        id = Selection.Value & ""

        On Error Resume Next
        Dim iID As New ClassIDCard
        ' 根据传入的内容GLobalIDStr更新文本
        Me.IDNumber.Caption = "身份证号：" & id

        If Len(id) <> 18 And Len(id) <> 15 Then
            Me.verify.Caption = "×"
            Me.verify.ForeColor = RGB(240, 0, 0)
            Call init   '显示默认的文字
            Exit Sub
        End If

        If iID.info(id, 7) Then
            Me.verify.Caption = "√"
            Me.verify.ForeColor = RGB(0, 130, 0)
        Else
            Me.verify.Caption = "×"
            Me.verify.ForeColor = RGB(240, 0, 0)
        End If

        Me.Sex.Caption = "性别：" & iID.info(id, 6)
        Me.age.Caption = "年龄：" & iID.info(id, 3) & "周岁"
        Me.xingzuo.Caption = "星座：" & iID.info(id, 5)
        Me.shuxiang.Caption = "属相：" & iID.info(id, 4)
        Me.birthday.Caption = "出生：" & Format(iID.info(id, 2), "YYYY年MM月DD日")
        Me.jiguan.Caption = iID.info(id, 1)
        
    Else
        Me.tipsLabel.ForeColor = VBA.RGB(255, 10, 10)
        Me.tipsLabel.Caption = "只能选择一个单元格"
    End If

End Sub
Private Sub init()
    ' 恢复为初始状态
    Me.IDNumber.Caption = "身份证号：未知"
    Me.Sex.Caption = "性别：未知"
    Me.age.Caption = "年龄：未知"
    Me.xingzuo.Caption = "星座：未知"
    Me.shuxiang.Caption = "属相：未知"
    Me.birthday.Caption = "出生：未知"
    Me.jiguan.Caption = "未知"
    Me.tipsLabel.ForeColor = VBA.RGB(100, 100, 100)
    Me.tipsLabel.Caption = "双击复制文本内容"
End Sub


' 双击复制
Private Sub age_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    putTextInClipboard (Me.age.Caption)
End Sub
Private Sub birthday_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.birthday.Caption)
End Sub
Private Sub xingzuo_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.xingzuo.Caption)
End Sub
Private Sub IDNumber_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.IDNumber.Caption)
End Sub
Private Sub Sex_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.Sex.Caption)
End Sub
Private Sub jiguan_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.jiguan.Caption, "籍贯")
End Sub
Private Sub shuxiang_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.shuxiang.Caption)
End Sub
Private Sub lunar_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.lunar.Caption)
End Sub

Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

    Dim str As String
    
    str = Me.IDNumber.Caption & Chr(13) & _
      Me.Sex.Caption & Chr(13) & _
      Me.age.Caption & Chr(13) & _
      Me.xingzuo.Caption & Chr(13) & _
      Me.shuxiang.Caption & Chr(13) & _
      Me.birthday.Caption & Chr(13) & _
      "籍贯：" & Me.jiguan.Caption & Chr(13)
      
    Call putTextInClipboard(str, "全部")
    
End Sub

' 写入内容到剪贴板
Public Sub putTextInClipboard(str As String, Optional ctr As String = "")

    ' 写入到剪贴板
    If ctr = "籍贯" Or ctr = "全部" Then
        Call CopyToClipboard(str)
    Else
        Dim temp As String
        temp = Split(str, "：")(1)
        Call CopyToClipboard(temp)
    End If

    ' 显示提示文字
    Dim ctrName As String
    If ctr = "" Then
        ctrName = Split(str, "：")(0)
    Else
        ctrName = ctr
    End If
    Me.copyed.Caption = Chr(13) & ctrName & Chr(13) & "已复制"
    Me.copyed.Visible = True
    Call Sleep(0.5)
    Me.copyed.Visible = False

End Sub

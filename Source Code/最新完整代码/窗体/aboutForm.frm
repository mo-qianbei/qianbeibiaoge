VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} aboutForm 
   Caption         =   "浅北表格 - 关于"
   ClientHeight    =   7830
   ClientLeft      =   120
   ClientTop       =   470
   ClientWidth     =   9360
   OleObjectBlob   =   "aboutForm.frx":0000
   StartUpPosition =   1  '所有者中心
End
Attribute VB_Name = "aboutForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'定义当前版本
Const thisVersion As Date = #10/6/2024#
Private addInName As String

Private Sub AddToAddins_Click()
    AddIns.Add FileName:=ThisWorkbook.FullName
    AddIns(addInName).Installed = True
    MsgBox "已添加到加载项！" & Chr(13) & "删除、移动、重命名此文件会导致加载项失效，需要重新添加！" & Chr(13) & "文件路径为：" & ThisWorkbook.FullName, vbInformation
End Sub

Private Sub LabelGitee_Click()
    ActiveWorkbook.FollowHyperlink address:="https://gitee.com/mo-qianbei/qianbeibiaoge", NewWindow:=True
End Sub

Private Sub LabelYueQue_Click()
    ActiveWorkbook.FollowHyperlink address:="https://www.yuque.com/moqianbei/qianbeibiaoge", NewWindow:=True
End Sub

Private Sub RemoveFromAddins_Click()
    If MsgBox("是否确认将「" & addInName & "」从加载项中禁用？", vbOKCancel + vbQuestion) = vbOK Then
        On Error Resume Next
        addInName = Replace(ThisWorkbook.Name, ".xlam", "")
        AddIns(addInName).Installed = False
        MsgBox "已禁用" & addInName
        Exit Sub
errLine:
        MsgBox "还未启用此插件，无需禁用"
    Else
        Exit Sub
    End If

End Sub

Private Sub UnlockCommandButton_Click()
    Call Fun.loadFun
End Sub

Private Sub Update_CommandButton_Click()

    Dim json As String, versionDate As Date, versionMsg As String
    json = getHttpJson("https://gitee.com/mo-qianbei/qianbeibiaoge/raw/master/version")

    versionDate = CDate(DoRegExp(json, "更新时间：([\d\-\\\/]{10})"))
    versionMsg = DoRegExp(json, "(更新内容：[\s\S]*)")

    '如果当前版本比要更新的版本高，退出程序
    If thisVersion >= versionDate Then
        MsgBox "已为最新版本，最近更新日期" & Format(thisVersion, "yyyy-MM-dd"), vbOKOnly, "检测更新"
    ElseIf MsgBox(Format(versionDate, "yyyy-MM-dd") & "发行了新版本，" & versionMsg & Chr(13) & "要现在升级吗？", vbYesNo, "检测更新") = vbYes Then
        ActiveWorkbook.FollowHyperlink address:="https://www.yuque.com/moqianbei/qianbeibiaoge/download", NewWindow:=True
    End If

End Sub

Private Function getHttpJson(ByVal url As String)

    Dim xHttp As Object
    Set xHttp = CreateObject("Microsoft.XMLHTTP")

    xHttp.Open "GET", url, False
    xHttp.send

    getHttpJson = xHttp.responsetext

End Function
Private Function DoRegExp(ByVal sOrignText As String, ByVal sPattern As String) As String

    Dim oRegExp As Object
    Set oRegExp = CreateObject("VBScript.Regexp")
    With oRegExp
        .Global = True      '匹配所有的符合项
        .IgnoreCase = True  '不区分大小写
        .Pattern = sPattern '正则规则

        '判断是否可以找到匹配的字符，若可以则返回True
        If .test(sOrignText) Then
'           '对字符串执行正则查找，返回所有的查找值的集合，若未找到，则为空
            Dim oMatches As Object
            '定义匹配子字符串集合对象
            Dim oSubMatches As Object
            Dim oMatch As Object

            Set oMatches = .Execute(sOrignText)
            For Each oMatch In oMatches
                DoRegExp = oMatch.SubMatches(0)
            Next
        Else
            DoRegExp = ""
        End If
    End With

    Set oRegExp = Nothing
    Set oMatches = Nothing
End Function

Private Sub UserForm_Initialize()
    Me.updateDate.Caption = "此版：" & Format(thisVersion, "yyyy-mm-dd")
    addInName = Replace(ThisWorkbook.Name, ".xlam", "")
End Sub

VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} CellsSizeForm 
   Caption         =   "选区行高与列宽调整"
   ClientHeight    =   5780
   ClientLeft      =   110
   ClientTop       =   460
   ClientWidth     =   7700
   OleObjectBlob   =   "CellsSizeForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  '所有者中心
End
Attribute VB_Name = "CellsSizeForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private shtEvent As classSheetEvent        ' 定义变量，在整个窗体加载过程中都存在

Private Sub UserForm_Deactivate()
    Set shtEvent = Nothing
End Sub


Private Sub UserForm_Initialize()

    Dim rowCMPXList, colCMPXList
    rowCMPXList = Array("磅", "厘米", "毫米", "英寸")
    colCMPXList = Array("字符", "磅", "厘米", "毫米", "英寸")
    
    Me.RowHeightFixComboBox.List = rowCMPXList
    Me.RowHeightFixComboBox.ListIndex = 0
    Me.RowHeightAddComboBox.List = rowCMPXList
    Me.RowHeightAddComboBox.ListIndex = 0
    
    Me.ColWidthFixComboBox.List = colCMPXList
    Me.ColWidthFixComboBox.ListIndex = 0
    Me.ColWidthAddComboBox.List = colCMPXList
    Me.ColWidthAddComboBox.ListIndex = 0
    
    ' 默认不调整行高列宽
    Me.RowHeightNoneOptionButton.Value = True
    Me.ColWidthNoneOptionButton.Value = True
     
    ' 因为在加载前，还没有添加事件，因此需要先获取一次
    Me.RangeTextBox.Value = Selection.address(0, 0)
    
    Set shtEvent = New classSheetEvent
    Set shtEvent.Worksheet = ActiveSheet

End Sub

Private Sub ApplyCommandButton_Click()
    Dim rng As Range
    Set rng = Range(Me.RangeTextBox.Value)

    Dim i As Long, j As Long
    Dim colWidth As Single
    On Error GoTo errLine
    For i = 1 To rng.Rows.Count
        Select Case True
            Case Me.RowHeightAddOptionButton
                rng.Rows(i).RowHeight = rng.Rows(i).RowHeight + cmpxToPt(Me.RowHeightAddTextBox.Value, Me.RowHeightAddComboBox.Value)

            Case Me.RowHeightFixOptionButton

                If Me.RowHeightFixComboBox.Value = "厘米" And Me.RowHeightFixTextBox.Value > 14.42 Then
                    MsgBox "行高必须在 0 ~ 14.42厘米"
                    Exit Sub
                ElseIf Me.RowHeightFixComboBox.Value = "毫米" And Me.RowHeightFixTextBox.Value > 144.29 Then
                    MsgBox "行高必须在 0 ~ 144.29毫米"
                    Exit Sub
                ElseIf Me.RowHeightFixComboBox.Value = "英寸" And Me.RowHeightFixTextBox.Value > 5.68 Then
                    MsgBox "行高必须在 0 ~ 5.68英寸"
                    Exit Sub
                ElseIf Me.RowHeightFixComboBox.Value = "磅" And Me.RowHeightFixTextBox.Value > 409 Then
                    MsgBox "行高必须在 0 ~ 409磅"
                    Exit Sub
                Else
                    rng.Rows(i).RowHeight = cmpxToPt(Me.RowHeightFixTextBox.Value, Me.RowHeightFixComboBox.Value)
                End If
            Case Me.RowHeightScaleOptionButton
                rng.Rows(i).RowHeight = rng.Rows(i).RowHeight * Me.RowHeightScaleTextBox.Value
            Case Me.RowHeightAutoOptionButton
                rng.Rows(i).AutoFit
            Case Me.RowHeightNoneOptionButton
        End Select
    Next
    
    For j = 1 To rng.Columns.Count
            Select Case True
            Case Me.ColWidthAddOptionButton
                If Me.ColWidthAddComboBox.Value = "字符" Then
                    colWidth = Me.ColWidthAddTextBox.Value
                Else
                    colWidth = widthToColumnWidth(cmpxToPt(Me.ColWidthAddTextBox.Value, Me.ColWidthAddComboBox.Value))
                End If
                rng.Columns(j).ColumnWidth = rng.Columns(j).ColumnWidth + colWidth
            Case Me.ColWidthFixOptionButton
                If Me.ColWidthFixComboBox.Value = "厘米" And Me.ColWidthFixTextBox.Value > 54.11 Then
                    MsgBox "行高必须在 0 ~ 54.11厘米"
                    Exit Sub
                ElseIf Me.ColWidthFixComboBox.Value = "毫米" And Me.ColWidthFixTextBox.Value > 541.14 Then
                    MsgBox "行高必须在 0 ~ 541.14毫米"
                    Exit Sub
                ElseIf Me.ColWidthFixComboBox.Value = "英寸" And Me.ColWidthFixTextBox.Value > 21.3 Then
                    MsgBox "行高必须在 0 ~ 21.3英寸"
                    Exit Sub
                ElseIf Me.ColWidthFixComboBox.Value = "磅" And Me.ColWidthFixTextBox.Value > 1533.83 Then
                    MsgBox "行高必须在 0 ~ 1533.83磅"
                    Exit Sub
                ElseIf Me.ColWidthFixComboBox.Value = "字符" And Me.ColWidthFixTextBox.Value > 255 Then
                    MsgBox "行高必须在 0 ~ 255字符"
                    Exit Sub
                Else
                    If Me.ColWidthFixComboBox.Value = "字符" Then
                        colWidth = Me.ColWidthFixTextBox.Value
                    Else
                        colWidth = widthToColumnWidth(cmpxToPt(Me.ColWidthFixTextBox.Value, Me.ColWidthFixComboBox.Value))
                    End If
                    rng.Columns(j).ColumnWidth = colWidth
                End If
            Case Me.ColWidthScaleOptionButton
                rng.Columns(j).ColumnWidth = rng.Columns(j).ColumnWidth * Me.ColWidthScaleTextBox.Value
            Case Me.ColWidthAutoOptionButton
                rng.Columns(j).AutoFit
            Case Me.ColWidthNoneOptionButton
        End Select
    Next
    Exit Sub

errLine:
    MsgBox "当前值超出可设定范围，请稍后重试"
End Sub


Private Sub QuitCommandButton_Click()
    Unload Me
End Sub


' ===== 按钮文本同步 ========
Private Sub ColWidthAddComboBox_Change()
    Me.ColWidthFixComboBox.Value = Me.ColWidthAddComboBox.Value
    Me.ColWidthAddOptionButton.Value = True
End Sub
Private Sub ColWidthFixComboBox_Change()
    Me.ColWidthAddComboBox.Value = Me.ColWidthFixComboBox.Value
    Me.ColWidthFixOptionButton.Value = True
End Sub
Private Sub RowHeightAddComboBox_Change()
    Me.RowHeightFixComboBox.Value = Me.RowHeightAddComboBox.Value
    Me.RowHeightAddOptionButton.Value = True
End Sub
Private Sub RowHeightFixComboBox_Change()
    Me.RowHeightAddComboBox.Value = Me.RowHeightFixComboBox.Value
    Me.RowHeightFixOptionButton.Value = True
End Sub


Function cmpxToPt(ByVal v, ByVal inCMPX As String)

    Select Case LCase(inCMPX)
    Case "pt", "磅"
        cmpxToPt = v
    Case "cm", "厘米"
        cmpxToPt = v / 0.035277778 ' 近似值，实际转换可能有细微差别
    Case "mm", "毫米"
        cmpxToPt = v / 0.35277778
    Case "inch", "英寸"
        cmpxToPt = v * 72
    End Select

End Function

' 将磅值转换为字符宽度
Function widthToColumnWidth(ByVal w As Single)
    Excel.Application.ScreenUpdating = False
    Dim rng As Range
    Set rng = Range(Me.RangeTextBox.Value).Cells(1)
    
    ' 存储当前状态：单元格列宽
    Dim rngColWidth As Single
    rngColWidth = rng.EntireColumn.ColumnWidth
    
    Dim gap As Single   ' 颗粒度，越小效率越低
    gap = 0.1

    ' 默认假设1字符 = 8pt
    Dim startColumnWidth As Single
    startColumnWidth = w / 8
    rng.EntireColumn.ColumnWidth = startColumnWidth

    Do While rng.width < w
        rng.EntireColumn.ColumnWidth = rng.EntireColumn.ColumnWidth + gap
    Loop
    Do While rng.width > w
        rng.EntireColumn.ColumnWidth = rng.EntireColumn.ColumnWidth - gap
    Loop

    widthToColumnWidth = rng.EntireColumn.ColumnWidth
 
    ' 获取后恢复
    rng.EntireColumn.ColumnWidth = rngColWidth
    
    Excel.Application.ScreenUpdating = True
End Function


' 聚焦时选中对应单选框
Private Sub ColWidthAddTextBox_Change()
    Me.ColWidthAddOptionButton.Value = True
End Sub
Private Sub ColWidthAddTextBox_Enter()
    Me.ColWidthAddOptionButton.Value = True
End Sub

Private Sub ColWidthFixTextBox_Change()
    Me.ColWidthFixOptionButton.Value = True
End Sub
Private Sub ColWidthFixTextBox_Enter()
    Me.ColWidthFixOptionButton.Value = True
End Sub

Private Sub ColWidthScaleTextBox_Change()
    Me.ColWidthScaleOptionButton.Value = True
End Sub
Private Sub ColWidthScaleTextBox_Enter()
    Me.ColWidthScaleOptionButton.Value = True
End Sub

Private Sub RowHeightAddTextBox_Change()
    Me.RowHeightAddOptionButton.Value = True
End Sub
Private Sub RowHeightAddTextBox_Enter()
    Me.RowHeightAddOptionButton.Value = True
End Sub

Private Sub RowHeightFixTextBox_Change()
    Me.RowHeightFixOptionButton.Value = True
End Sub
Private Sub RowHeightFixTextBox_Enter()
    Me.RowHeightFixOptionButton.Value = True
End Sub

Private Sub RowHeightScaleTextBox_Change()
    Me.RowHeightScaleOptionButton.Value = True
End Sub
Private Sub RowHeightScaleTextBox_Enter()
    Me.RowHeightScaleOptionButton.Value = True
End Sub

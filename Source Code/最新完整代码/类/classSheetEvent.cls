VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "classSheetEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public WithEvents Worksheet As Worksheet
Attribute Worksheet.VB_VarHelpID = -1

Private Sub Worksheet_SelectionChange(ByVal Target As Range)
    ' 因为两者大概率不会同时出现，这里就不做区分了
    If CellsSizeForm.Visible = True Then
        CellsSizeForm.RangeTextBox.Value = Target.address(0, 0)
    End If
    If IDCardForm.Visible = True Then
        IDCardForm.RangeTextBox.Value = Target.address(0, 0)
    End If
End Sub

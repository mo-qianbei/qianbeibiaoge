VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ClassIDCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


'提取身份证信息
Property Get info(id As String, Optional getType As Integer = 7) As Variant
'ID不为15位或18位，返回空
'1为地区，未找到返回空
'2为真正出生日期，如果该数字错误返回，2及以下值均返回错误提示字符串
    '3为年龄（周岁数字）
    '4为生肖
    '5为星座
' 6 为性别：男女
' 7 判断校验码是否正确：布尔值Boolean
' 8 获取真正校验码
' 9 转18位身份证号

    info = "身份证号不符合规范"   '设定默认返回值
    
    If Len(id) = 15 Then
        info = info(ToRule(id), getType)
        
    ElseIf Len(id) = 18 Then

        Dim errDataMsg As String, errPlaceMsg As String
        errDataMsg = "身份证出生日期有误"
        errPlaceMsg = "找不到身份证所在地区"

        Select Case getType
            Case 1   '1为地区
                Dim pre6 As String
                pre6 = Left(id, 6)
                On Error Resume Next
                '单元格匹配
                Dim rng As Range
                Set rng = premierSheet0.Range("A:A").Find(What:=pre6, LookAt:=xlWhole)
                info = rng.Offset(0, 2).Value & rng.Offset(0, 3).Value & rng.Offset(0, 4).Value
                If Err.Number = 91 Then
                    info = errPlaceMsg
                    Err.Clear
                End If
                On Error GoTo 0

            Case 2 '出生日期转换为真正日期
                If Format(DateSerial(Mid(id, 7, 4), Mid(id, 11, 2), Mid(id, 13, 2)), "yyyymmdd") = Mid(id, 7, 8) Then
                    info = DateSerial(Mid(id, 7, 4), Mid(id, 11, 2), Mid(id, 13, 2))
                Else
                    info = errDataMsg
                End If
    
            Case 3   '3为年龄
                If info(id, 2) <> errDataMsg Then
                    info = DateDiff("yyyy", info(id, 2), Date)
                Else
                    info = errDataMsg
                End If
            Case 4   '4为生肖
                Dim shengxiao() As Variant
                shengxiao = Array("鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪")

                If info(id, 2) <> errDataMsg Then
                    info = shengxiao((Mid(id, 7, 4) - 1900) Mod 12) '1900年为鼠年
                Else
                    info = errDataMsg
                End If
            Case 5   '5为星座
                If info(id, 2) <> errDataMsg Then
                    Dim xingzuo
                    xingzuo = [{"水瓶座",0120,0218;"双鱼座",0219,0320;"白羊座",0321,0419;"金牛座",0420,0520;"双子座",0521,0621;"巨蟹座",0622,0722;"狮子座",0723,0822;"处女座",0823,0922;"天秤座",0923,1023;"天蝎座",1024,1122;"射手座",1123,1221;"摩羯座",1222,1231;"摩羯座",0101,0119}]
                    Dim id4
                    id4 = Val(Mid(id, 11, 4)) '转为1000四位数进行比较
                    Dim i As Byte
                    For i = 1 To 13
                        If id4 >= xingzuo(i, 2) And id4 <= xingzuo(i, 3) Then
                            info = xingzuo(i, 1)
                            Exit For
                        End If
                    Next
                Else
                    info = errDataMsg
                End If
            Case 6   '6为性别
                info = IIf(Mid(id, 17, 1) Mod 2, "男", "女")
            Case 7   '7判断校验码是否正确
                If CRC(id) = UCase(Right(id, 1)) Then
                    info = True
                Else
                    info = False
                End If
            Case 8  '校验码
                info = CRC(id)
            Case 9  '转18位号码
                info = ToRule(id)
            Case Else

        End Select
    Else

    End If

End Property
' 转换为规范的18位身份证号码
Property Get ToRule(id As String)
'如果ID长度不为15，返回原值
'否则返回18位字符

    If Len(id) <> 15 Then
        ToRule = id
    Else
        Dim pre17 As String
        pre17 = Left(id, 6) & "19" & Right(id, 9)
        ToRule = pre17 & CRC(pre17)
    End If

End Property

Property Get CRC(id As String) As String
'根据前17位数字，返回身份证校验码
'如果ID长度不是17或18位，返回""
'正常返回值可能为[0-9X]

    If Len(id) = 17 Or Len(id) = 18 Then
        Dim i As Byte, sum As Long
        For i = 1 To 17
            sum = sum + ((2 ^ (18 - i)) Mod 11) * Mid(id, i, 1)
        Next
        Dim remainder As Long   '余数
        remainder = sum Mod 11
        '0,1,2,3,4,5,6,7,8,9,10 => 1,0,X,9,8,7,6,5,4,3,2
        'm：0－1－2－3－4－5－6－7－8－9——10（10-m）↓
        'n：X－9－8－7－6－5－4－3－2－1－0(n + 2) mod 11 ↓
        'r: 1－0－X－9－8－7－6－5－4－3－2
        If (12 - remainder) Mod 11 = 10 Then
            CRC = "X"
        Else
            CRC = (12 - remainder) Mod 11 & ""
        End If
    Else
        CRC = ""
    End If
End Property


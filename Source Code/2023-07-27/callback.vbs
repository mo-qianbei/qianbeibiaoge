' 其他更新内容可查看往期，多是一些体验优化，不再赘述

' 批量添加超链接
Sub addHyperLinks(control As IRibbonControl)
    On Error GoTo errLine
    Dim sel As Range
    Set sel = Intersect(Selection, ActiveSheet.UsedRange)   ' 避免选中未空白单元格而出错
    If sel.Columns.Count > 1 And sel.Rows.Count > 1 Then
        MsgBox "请先选择单列/行的单元格"
    ElseIf sel.Columns.Count = 1 And sel.Rows.Count = 1 Then
        MsgBox "此操作需要选中至少两个单元格"
    Else
        Dim inpRng As Range
        Set inpRng = Excel.Application.InputBox("请选中超链接地址的任一单元格，程序会自动判断", Type:=8)
        Dim offCol As Long, offRow As Long
        If sel.Columns.Count = 1 Then
            offCol = inpRng.Column - sel.Column
            offRow = 0
        ElseIf sel.Rows.Count = 1 Then
            offRow = inpRng.Row - sel.Row
            offCol = 0
        End If
        Dim rng As Range
        For Each rng In sel
            ActiveSheet.Hyperlinks.Add Anchor:=rng, Address:=rng.Offset(offRow, offCol).Value
        Next
    End If

    Exit Sub
    
errLine:
    MsgBox "程序已终止！请按如下步骤重试：" & Chr(13) & _
      "  1.选中要展示文字所在的多个单元格（单列/行）" & Chr(13) & _
      "  2.点击此功能按钮" & Chr(13) & _
      "  3.选中超链接地址所在任一单元格"
End Sub
VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} IDCardForm 
   Caption         =   "身份证号信息查询"
   ClientHeight    =   4080
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   4440
   OleObjectBlob   =   "IDCardForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  '所有者中心
End
Attribute VB_Name = "IDCardForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub CommandButton1_Click()
    Unload Me
End Sub


Private Sub UserForm_Initialize()
    On Error Resume Next
    Dim iID As New ClassIDCard
    ' 根据传入的内容GLobalIDStr更新文本
    Me.IDNumber.Caption = "身份证号：" & GLobalIDStr
    If iID.info(GLobalIDStr, 7) Then
        Me.verify.Caption = "√"
        Me.verify.ForeColor = RGB(0, 130, 0)
    Else
        Me.verify.Caption = "×"
        Me.verify.ForeColor = RGB(240, 0, 0)
    End If

    Me.Sex.Caption = "性别：" & iID.info(GLobalIDStr, 6)
    Me.age.Caption = "年龄：" & iID.info(GLobalIDStr, 3) & "周岁"
    Me.xingzuo.Caption = "星座：" & iID.info(GLobalIDStr, 5)
    Me.shuxiang.Caption = "属相：" & iID.info(GLobalIDStr, 4)
    Me.birthday.Caption = "出生：" & Format(iID.info(GLobalIDStr, 2), "YYYY年MM月DD日")
    Me.jiguan.Caption = iID.info(GLobalIDStr, 1)
    
End Sub

Private Sub age_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    putTextInClipboard (Me.age.Caption)
End Sub

Private Sub birthday_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.birthday.Caption)
End Sub

Private Sub xingzuo_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.xingzuo.Caption)
End Sub

Private Sub IDNumber_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.IDNumber.Caption)
End Sub

Private Sub Sex_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.Sex.Caption)
End Sub

Private Sub jiguan_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.jiguan.Caption, "籍贯")
End Sub
Private Sub shuxiang_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.shuxiang.Caption)
End Sub
Private Sub lunar_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call putTextInClipboard(Me.lunar.Caption)
End Sub
Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Dim str As String
    
    str = Me.IDNumber.Caption & Chr(13) & _
      Me.Sex.Caption & Chr(13) & _
      Me.age.Caption & Chr(13) & _
      Me.xingzuo.Caption & Chr(13) & _
      Me.shuxiang.Caption & Chr(13) & _
      Me.birthday.Caption & Chr(13) & _
      "籍贯：" & Me.jiguan.Caption & Chr(13)
      
    Call putTextInClipboard(str, "全部")
    
End Sub

' 写入内容到剪贴板
Private Sub putTextInClipboard(str As String, Optional ctr As String = "")

    ' 写入到剪贴板
    Dim CLP
    Set CLP = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    If ctr = "籍贯" Or ctr = "全部" Then
        CLP.SetText str
    Else
        CLP.SetText Split(str, "：")(1)
    End If
    CLP.PutInClipboard

    ' 显示提示文字
    Dim ctrName As String
    If ctr = "" Then
        ctrName = Split(str, "：")(0)
    Else
        ctrName = ctr
    End If
    Me.copyed.Caption = Chr(13) & ctrName & Chr(13) & "已复制"
    Me.copyed.Visible = True
    Call Sleep(0.5)
    Me.copyed.Visible = False
    
End Sub


' s 暂停的秒数，1秒 = 1000毫秒
Sub Sleep(Optional ByVal s As Single = 1)

    Dim StartTime As Single
    StartTime = Timer   ' 获取今天过了多少秒
    Dim StartDate As Date
    StartDate = Date    ' 获取当前日期

    Do While 86400 * (Date - StartDate) + Timer < StartTime + s
        DoEvents
    Loop
End Sub

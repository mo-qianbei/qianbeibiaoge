VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} aboutForm 
   Caption         =   "浅北表格 - 关于"
   ClientHeight    =   7810
   ClientLeft      =   120
   ClientTop       =   470
   ClientWidth     =   9680.001
   OleObjectBlob   =   "aboutForm.frx":0000
   StartUpPosition =   1  '所有者中心
End
Attribute VB_Name = "aboutForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub LabelGitee_Click()
    ActiveWorkbook.FollowHyperlink Address:="https://gitee.com/mo-qianbei/qianbeibiaoge", NewWindow:=True
End Sub

Private Sub LabelYueQue_Click()
    ActiveWorkbook.FollowHyperlink Address:="https://www.yuque.com/moqianbei/qianbeibiaoge", NewWindow:=True
End Sub

Private Sub UnlockCommandButton_Click()
    Call loadFun
End Sub

Private Sub Update_CommandButton_Click()

    '定义当前版本
    Const thisVersion = #4/29/2022#

    Dim json As String, versionDate As Date, versionMsg As String
    json = getHttpJson("https://gitee.com/mo-qianbei/qianbeibiaoge/raw/master/version")

    versionDate = CDate(DoRegExp(json, "更新时间：([\d\-\\\/]{10})"))
    versionMsg = DoRegExp(json, "(更新内容：[\s\S]*)")

    '如果当前版本比要更新的版本高，退出程序
    If thisVersion >= versionDate Then
        MsgBox "已为最新版本，最近更新日期" & Format(thisVersion, "yyyy-MM-dd"), vbOKOnly, "检测更新"
    ElseIf MsgBox(Format(versionDate, "yyyy-MM-dd") & "发行了新版本，" & versionMsg & Chr(13) & "要现在升级吗？", vbYesNo, "检测更新") = vbYes Then
        ActiveWorkbook.FollowHyperlink Address:="https://gitee.com/mo-qianbei/qianbeibiaoge/releases", NewWindow:=True
    End If

End Sub

Private Sub UserForm_Click()

End Sub
